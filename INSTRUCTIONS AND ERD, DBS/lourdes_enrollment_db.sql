-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2018 at 02:09 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lourdes_enrollment_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE `tbl_countries` (
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`country_code`, `country_name`) VALUES
('AD', 'Andorra'),
('AE', 'United Arab Emirates'),
('AF', 'Afghanistan'),
('AG', 'Antigua and Barbuda'),
('AI', 'Anguilla'),
('AL', 'Albania'),
('AM', 'Armenia'),
('AN', 'Netherlands Antilles'),
('AO', 'Angola'),
('AQ', 'Antarctica'),
('AR', 'Argentina'),
('AT', 'Austria'),
('AU', 'Australia'),
('AW', 'Aruba'),
('AZ', 'Azerbaijan'),
('BA', 'Bosnia and Herzegovina'),
('BB', 'Barbados'),
('BD', 'Bangladesh'),
('BE', 'Belgium'),
('BF', 'Burkina Faso'),
('BG', 'Bulgaria'),
('BH', 'Bahrain'),
('BI', 'Burundi'),
('BJ', 'Benin'),
('BM', 'Bermuda'),
('BN', 'Brunei Darussalam'),
('BO', 'Bolivia'),
('BR', 'Brazil'),
('BS', 'Bahamas'),
('BT', 'Bhutan'),
('BV', 'Bouvet Island'),
('BW', 'Botswana'),
('BY', 'Belarus'),
('BZ', 'Belize'),
('CA', 'Canada'),
('CC', 'Cocos (Keeling) Islands'),
('CF', 'Central African Republic'),
('CG', 'Congo'),
('CH', 'Switzerland'),
('CI', 'Ivory Coast'),
('CK', 'Cook Islands'),
('CL', 'Chile'),
('CM', 'Cameroon'),
('CN', 'China'),
('CO', 'Colombia'),
('CR', 'Costa Rica'),
('CU', 'Cuba'),
('CV', 'Cape Verde'),
('CX', 'Christmas Island'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('DE', 'Germany'),
('DJ', 'Djibouti'),
('DK', 'Denmark'),
('DM', 'Dominica'),
('DO', 'Dominican Republic'),
('DS', 'American Samoa'),
('DZ', 'Algeria'),
('EC', 'Ecuador'),
('EE', 'Estonia'),
('EG', 'Egypt'),
('EH', 'Western Sahara'),
('ER', 'Eritrea'),
('ES', 'Spain'),
('ET', 'Ethiopia'),
('FI', 'Finland'),
('FJ', 'Fiji'),
('FK', 'Falkland Islands (Malvinas)'),
('FM', 'Micronesia, Federated States of'),
('FO', 'Faroe Islands'),
('FR', 'France'),
('FX', 'France, Metropolitan'),
('GA', 'Gabon'),
('GB', 'United Kingdom'),
('GD', 'Grenada'),
('GE', 'Georgia'),
('GF', 'French Guiana'),
('GH', 'Ghana'),
('GI', 'Gibraltar'),
('GK', 'Guernsey'),
('GL', 'Greenland'),
('GM', 'Gambia'),
('GN', 'Guinea'),
('GP', 'Guadeloupe'),
('GQ', 'Equatorial Guinea'),
('GR', 'Greece'),
('GS', 'South Georgia South Sandwich Islands'),
('GT', 'Guatemala'),
('GU', 'Guam'),
('GW', 'Guinea-Bissau'),
('GY', 'Guyana'),
('HK', 'Hong Kong'),
('HM', 'Heard and Mc Donald Islands'),
('HN', 'Honduras'),
('HR', 'Croatia (Hrvatska)'),
('HT', 'Haiti'),
('HU', 'Hungary'),
('ID', 'Indonesia'),
('IE', 'Ireland'),
('IL', 'Israel'),
('IM', 'Isle of Man'),
('IN', 'India'),
('IO', 'British Indian Ocean Territory'),
('IQ', 'Iraq'),
('IR', 'Iran (Islamic Republic of)'),
('IS', 'Iceland'),
('IT', 'Italy'),
('JE', 'Jersey'),
('JM', 'Jamaica'),
('JO', 'Jordan'),
('JP', 'Japan'),
('KE', 'Kenya'),
('KG', 'Kyrgyzstan'),
('KH', 'Cambodia'),
('KI', 'Kiribati'),
('KM', 'Comoros'),
('KN', 'Saint Kitts and Nevis'),
('KP', 'Korea, Democratic People\'s Republic of'),
('KR', 'Korea, Republic of'),
('KW', 'Kuwait'),
('KY', 'Cayman Islands'),
('KZ', 'Kazakhstan'),
('LA', 'Lao People\'s Democratic Republic'),
('LB', 'Lebanon'),
('LC', 'Saint Lucia'),
('LI', 'Liechtenstein'),
('LK', 'Sri Lanka'),
('LR', 'Liberia'),
('LS', 'Lesotho'),
('LT', 'Lithuania'),
('LU', 'Luxembourg'),
('LV', 'Latvia'),
('LY', 'Libyan Arab Jamahiriya'),
('MA', 'Morocco'),
('MC', 'Monaco'),
('MD', 'Moldova, Republic of'),
('ME', 'Montenegro'),
('MG', 'Madagascar'),
('MH', 'Marshall Islands'),
('MK', 'Macedonia'),
('ML', 'Mali'),
('MM', 'Myanmar'),
('MN', 'Mongolia'),
('MO', 'Macau'),
('MP', 'Northern Mariana Islands'),
('MQ', 'Martinique'),
('MR', 'Mauritania'),
('MS', 'Montserrat'),
('MT', 'Malta'),
('MU', 'Mauritius'),
('MV', 'Maldives'),
('MW', 'Malawi'),
('MX', 'Mexico'),
('MY', 'Malaysia'),
('MZ', 'Mozambique'),
('NA', 'Namibia'),
('NC', 'New Caledonia'),
('NE', 'Niger'),
('NF', 'Norfolk Island'),
('NG', 'Nigeria'),
('NI', 'Nicaragua'),
('NL', 'Netherlands'),
('NO', 'Norway'),
('NP', 'Nepal'),
('NR', 'Nauru'),
('NU', 'Niue'),
('NZ', 'New Zealand'),
('OM', 'Oman'),
('PA', 'Panama'),
('PE', 'Peru'),
('PF', 'French Polynesia'),
('PG', 'Papua New Guinea'),
('PH', 'Philippines'),
('PK', 'Pakistan'),
('PL', 'Poland'),
('PM', 'St. Pierre and Miquelon'),
('PN', 'Pitcairn'),
('PR', 'Puerto Rico'),
('PS', 'Palestine'),
('PT', 'Portugal'),
('PW', 'Palau'),
('PY', 'Paraguay'),
('QA', 'Qatar'),
('RE', 'Reunion'),
('RO', 'Romania'),
('RS', 'Serbia'),
('RU', 'Russian Federation'),
('RW', 'Rwanda'),
('SA', 'Saudi Arabia'),
('SB', 'Solomon Islands'),
('SC', 'Seychelles'),
('SD', 'Sudan'),
('SE', 'Sweden'),
('SG', 'Singapore'),
('SH', 'St. Helena'),
('SI', 'Slovenia'),
('SJ', 'Svalbard and Jan Mayen Islands'),
('SK', 'Slovakia'),
('SL', 'Sierra Leone'),
('SM', 'San Marino'),
('SN', 'Senegal'),
('SO', 'Somalia'),
('SR', 'Suriname'),
('ST', 'Sao Tome and Principe'),
('SV', 'El Salvador'),
('SY', 'Syrian Arab Republic'),
('SZ', 'Swaziland'),
('TC', 'Turks and Caicos Islands'),
('TD', 'Chad'),
('TF', 'French Southern Territories'),
('TG', 'Togo'),
('TH', 'Thailand'),
('TJ', 'Tajikistan'),
('TK', 'Tokelau'),
('TM', 'Turkmenistan'),
('TN', 'Tunisia'),
('TO', 'Tonga'),
('TP', 'East Timor'),
('TR', 'Turkey'),
('TT', 'Trinidad and Tobago'),
('TV', 'Tuvalu'),
('TW', 'Taiwan'),
('TY', 'Mayotte'),
('TZ', 'Tanzania, United Republic of'),
('UA', 'Ukraine'),
('UG', 'Uganda'),
('UM', 'United States minor outlying islands'),
('US', 'United States'),
('UY', 'Uruguay'),
('UZ', 'Uzbekistan'),
('VA', 'Vatican City State'),
('VC', 'Saint Vincent and the Grenadines'),
('VE', 'Venezuela'),
('VG', 'Virgin Islands (British)'),
('VI', 'Virgin Islands (U.S.)'),
('VN', 'Vietnam'),
('VU', 'Vanuatu'),
('WF', 'Wallis and Futuna Islands'),
('WS', 'Samoa'),
('XK', 'Kosovo'),
('YE', 'Yemen'),
('ZA', 'South Africa'),
('ZM', 'Zambia'),
('ZR', 'Zaire'),
('ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_course`
--

CREATE TABLE `tbl_course` (
  `course_id` int(11) NOT NULL,
  `course` varchar(20) NOT NULL,
  `description` varchar(191) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_course`
--

INSERT INTO `tbl_course` (`course_id`, `course`, `description`, `updated_at`, `created_at`) VALUES
(1, 'BSA', 'Bachelor of Science in Accountancy', '2018-11-07 11:28:43', '0000-00-00 00:00:00'),
(2, 'BSIT', 'Bachelor of Science in Information Technology', '2018-11-07 11:28:43', '0000-00-00 00:00:00'),
(3, 'BSBA', 'Bachelor of Science in Business Administration - Financial Management', '2018-11-07 20:21:29', '0000-00-00 00:00:00'),
(5, 'BSIS', 'Bachelor of Science in Information System', '2018-11-07 19:33:48', '2018-11-07 19:33:48'),
(6, 'BSAIS', 'Bachelor of Science in Accounting Information System', '2018-11-07 20:09:09', '2018-11-07 20:09:09'),
(7, 'BSBA', 'Bachelor of Science in Business Administration - Marketing Management', '2018-11-07 20:22:09', '2018-11-07 20:22:09'),
(8, 'BSED - MF', 'Bachelor of Secondary Education Major in Filipino', '2018-11-07 20:23:50', '2018-11-07 20:23:50'),
(9, 'BSED - SS', 'Bachelor of Secondary Education Major in Social Studies', '2018-11-07 20:24:31', '2018-11-07 20:24:31'),
(10, 'BSED - MVE', 'Bachelor of Secondary Education Major in Values Education', '2018-11-07 20:25:22', '2018-11-07 20:25:22'),
(11, 'BCAE', 'Bachelor of Culture and Arts Education', '2018-11-07 20:25:53', '2018-11-07 20:25:53'),
(12, 'BTLE - HE', 'Bachelor of Technology & Livelihood Education Major in Home Economics', '2018-11-07 20:28:55', '2018-11-07 20:28:55'),
(13, 'BSED - ME', 'Bachelor of Secondary Education Major in English', '2018-11-07 20:29:25', '2018-11-07 20:29:25'),
(14, 'BECE', 'Bachelor of Early Childhood Education', '2018-11-07 20:29:53', '2018-11-07 20:29:53'),
(15, 'BEE', 'Bachelor of Elementary Education', '2018-11-07 20:30:07', '2018-11-07 20:30:07'),
(16, 'BSND', 'Bachelor of Science in Nutrition & Dietetics', '2018-11-07 20:30:29', '2018-11-07 20:30:29'),
(17, 'BSN', 'Bachelor in Science in Nursing', '2018-11-07 20:30:43', '2018-11-07 20:30:43'),
(18, 'BSPHARM', 'Bachelor of Science in Pharmacy', '2018-11-07 20:31:12', '2018-11-07 20:31:12'),
(19, 'BSTM', 'Bachelor of Science in Tourism Management', '2018-11-07 20:31:31', '2018-11-07 20:31:31'),
(20, 'BSHM', 'Bachelor of Science in Hospitality Management', '2018-11-07 20:32:46', '2018-11-07 20:32:46'),
(21, 'BA-English Language', 'Bachelor of Arts in English Language', '2018-11-07 20:33:12', '2018-11-07 20:33:12'),
(22, 'BA-Comm', 'Bachelor of Arts in Communication', '2018-11-07 20:33:25', '2018-11-07 20:33:25'),
(23, 'BS-PSYCH', 'Bachelor of Science in Psychology', '2018-11-07 20:33:59', '2018-11-07 20:33:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_faculty`
--

CREATE TABLE `tbl_faculty` (
  `faculty_id` int(11) NOT NULL,
  `faculty_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_faculty`
--

INSERT INTO `tbl_faculty` (`faculty_id`, `faculty_name`) VALUES
(2, 'Patrick Marlowe Oliva'),
(3, 'Luis Cadiz'),
(5, 'Johnbert Estroga'),
(7, 'Kurt Candillas'),
(8, 'Alexander Suan'),
(9, 'Loveth Mae Angcos'),
(11, 'Delia Pahang'),
(12, 'Rhandy Oyao'),
(13, 'Anthony Dagang'),
(14, 'Babes Reyes'),
(16, 'Rhea Ganas'),
(17, 'Noel Pit'),
(18, 'Rose Mary Cardenas'),
(19, 'Temporada'),
(20, 'Marilou Magadan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE `tbl_notification` (
  `notification_id` int(11) NOT NULL,
  `notification` text NOT NULL,
  `sent_from` varchar(255) NOT NULL,
  `sent_to` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_sent` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`notification_id`, `notification`, `sent_from`, `sent_to`, `type`, `status`, `date_sent`) VALUES
(81, 'Subjects you requested for enrollment evaluated successfully and already submitted to registrar for confirmation.', 'rhandy', 'jantinn.webdeveloper@gmail.com', 1, 0, '2018-11-11 15:17:44'),
(83, 'Subjects you requested for enrollment evaluated successfully and already submitted to registrar for confirmation.', 'rhandy', 'jantinn.webdeveloper@gmail.com', 1, 0, '2018-11-21 11:18:10'),
(87, 'Subjects you requested for enrollment evaluated successfully and already submitted to registrar for confirmation.', 'rhandy', 'jantinn.webdeveloper@gmail.com', 1, 0, '2018-11-21 12:35:53'),
(89, 'Subjects you requested for enrollment evaluated successfully and already submitted to registrar for confirmation.', 'rhandy', 'jantinn.webdeveloper@gmail.com', 1, 0, '2018-11-21 12:53:19'),
(90, 'You\'re enrollment request is already processed, please check your email for the payment details and instructions.', 'admin2018', 'jantinn.webdeveloper@gmail.com', 1, 0, '2018-11-21 12:54:15'),
(91, 'You are now enrolled!', 'admin2018', 'jantinn.webdeveloper@gmail.com', 1, 0, '2018-11-21 12:55:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_payment`
--

CREATE TABLE `tbl_payment` (
  `payment_id` int(11) NOT NULL,
  `payment_code` varchar(20) NOT NULL,
  `request_id` varchar(50) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_payment`
--

INSERT INTO `tbl_payment` (`payment_id`, `payment_code`, `request_id`, `payment_status`, `created_at`, `updated_at`) VALUES
(6, '998967359', '27,28', 1, '2018-11-11 03:52:02', '2018-11-10 15:52:02'),
(7, '915161207', '27,28', 1, '2018-11-11 03:57:02', '2018-11-10 15:57:02'),
(8, '213638123', '27,28', 1, '2018-11-11 03:59:50', '2018-11-10 15:59:50'),
(9, '858891849', '11,12', 1, '2018-11-21 12:55:52', '2018-11-21 00:55:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_prereq`
--

CREATE TABLE `tbl_prereq` (
  `prereq_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `prereqsubject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_schedule`
--

CREATE TABLE `tbl_schedule` (
  `schedule_id` int(11) NOT NULL,
  `schedule_day` varchar(50) NOT NULL,
  `semester` int(1) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `subject_id` int(11) NOT NULL,
  `room` varchar(100) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `slots` int(2) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_schedule`
--

INSERT INTO `tbl_schedule` (`schedule_id`, `schedule_day`, `semester`, `school_year`, `start_time`, `end_time`, `subject_id`, `room`, `faculty_id`, `slots`, `status`, `updated_at`, `created_at`) VALUES
(27, 'MTH', 1, '2018-2019', '09:00:00', '10:30:00', 145, 'CompLab', 2, 10, 1, '2018-11-11 03:41:30', '2018-11-08 18:36:20'),
(28, 'MTH', 1, '2018-2019', '01:00:00', '02:30:00', 143, '207', 9, 32, 1, '2018-11-21 12:52:13', '2018-11-08 18:37:02'),
(29, 'TF', 1, '2018-2019', '17:03:00', '19:00:00', 148, '800', 7, 42, 1, '2018-11-21 12:34:57', '2018-11-21 00:04:46'),
(30, 'SAT', 1, '2018-2019', '13:00:00', '16:00:00', 145, '250', 2, 45, 1, '2018-11-21 00:11:44', '2018-11-21 00:11:44'),
(31, 'WED', 1, '2018-2019', '10:30:00', '12:00:00', 147, '101', 11, 29, 1, '2018-11-21 12:52:13', '2018-11-21 00:47:25');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_semester`
--

CREATE TABLE `tbl_semester` (
  `semester` int(1) NOT NULL,
  `description` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_semester`
--

INSERT INTO `tbl_semester` (`semester`, `description`, `status`) VALUES
(1, 'First semester', 1),
(2, 'Second semester', 0),
(3, 'Summer', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `course_id` int(11) NOT NULL,
  `year_level` int(1) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `religion` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `place_of_birth` varchar(255) NOT NULL,
  `civil_status` varchar(10) NOT NULL,
  `home_address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `country_code` varchar(5) NOT NULL,
  `zipcode` int(10) NOT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `guardian` varchar(255) NOT NULL,
  `guardian_relationship` varchar(255) NOT NULL,
  `basic_education` varchar(255) NOT NULL,
  `secondary_education` varchar(255) NOT NULL,
  `college_education` varchar(255) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `enrolled` tinyint(1) NOT NULL,
  `date_enrolled` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`email`, `password`, `img`, `lastname`, `firstname`, `middlename`, `course_id`, `year_level`, `gender`, `religion`, `nationality`, `date_of_birth`, `place_of_birth`, `civil_status`, `home_address`, `city`, `province`, `country_code`, `zipcode`, `phone`, `guardian`, `guardian_relationship`, `basic_education`, `secondary_education`, `college_education`, `confirmed`, `enrolled`, `date_enrolled`) VALUES
('jantinn.webdeveloper@gmail.com', 'JohnWickkdYel2018', 'noimage.jpg', 'Wick', 'John', 'Klein', 1, 1, 'Male', 'Roman Catholic', 'Filipino', '2018-11-16', 'cdo', 'Single', 'dgdfg', 'Cebu City', 'fgdfgdfgdg', 'AD', 9000, NULL, 'fgf', 'fdgd', 'fgdfg', 'dfgdf', 'gdfgdf', 1, 0, '2018-11-11');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_studsubjects`
--

CREATE TABLE `tbl_studsubjects` (
  `studsubject_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `student_email` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_studsubjects`
--

INSERT INTO `tbl_studsubjects` (`studsubject_id`, `schedule_id`, `student_email`, `date_added`) VALUES
(1, 28, 'jantinn.webdeveloper@gmail.com', '2018-11-21 12:55:52'),
(2, 31, 'jantinn.webdeveloper@gmail.com', '2018-11-21 12:55:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subcourse`
--

CREATE TABLE `tbl_subcourse` (
  `subcourse_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subcourse`
--

INSERT INTO `tbl_subcourse` (`subcourse_id`, `subject_id`, `course_id`) VALUES
(1, 140, 2),
(2, 140, 3),
(3, 141, 1),
(4, 141, 2),
(5, 141, 3),
(6, 142, 1),
(7, 142, 2),
(8, 142, 3),
(9, 142, 5),
(10, 142, 6),
(11, 142, 7),
(12, 142, 8),
(13, 142, 9),
(14, 142, 10),
(15, 142, 11),
(16, 142, 12),
(17, 142, 13),
(18, 142, 14),
(19, 142, 15),
(20, 142, 16),
(21, 142, 17),
(22, 142, 18),
(23, 142, 19),
(24, 142, 20),
(25, 142, 21),
(26, 142, 22),
(27, 142, 23),
(28, 143, 1),
(29, 143, 2),
(30, 143, 3),
(31, 143, 5),
(32, 143, 6),
(33, 143, 7),
(34, 143, 8),
(35, 143, 9),
(36, 143, 10),
(37, 143, 11),
(38, 143, 12),
(39, 143, 13),
(40, 143, 14),
(41, 143, 15),
(42, 143, 16),
(43, 143, 17),
(44, 143, 18),
(45, 143, 19),
(46, 143, 20),
(47, 143, 21),
(48, 143, 22),
(49, 143, 23),
(50, 144, 1),
(51, 144, 2),
(52, 144, 3),
(53, 144, 5),
(54, 144, 6),
(55, 144, 7),
(56, 144, 8),
(57, 144, 9),
(58, 144, 10),
(59, 144, 11),
(60, 144, 12),
(61, 144, 13),
(62, 144, 14),
(63, 144, 15),
(64, 144, 16),
(65, 144, 17),
(66, 144, 18),
(67, 144, 19),
(68, 144, 20),
(69, 144, 21),
(70, 144, 22),
(71, 144, 23),
(72, 145, 2),
(73, 146, 1),
(74, 146, 2),
(75, 146, 3),
(76, 146, 7),
(77, 147, 1),
(78, 147, 2),
(79, 147, 3),
(80, 147, 5),
(81, 147, 6),
(82, 147, 7),
(83, 147, 8),
(84, 147, 9),
(85, 147, 10),
(86, 147, 11),
(87, 147, 12),
(88, 147, 13),
(89, 147, 14),
(90, 147, 15),
(91, 147, 16),
(92, 147, 17),
(93, 147, 18),
(94, 147, 19),
(95, 147, 20),
(96, 147, 21),
(97, 147, 22),
(98, 147, 23),
(99, 148, 1),
(100, 148, 2),
(101, 148, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subjects`
--

CREATE TABLE `tbl_subjects` (
  `subject_id` int(11) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `descriptive` varchar(255) NOT NULL,
  `lec` varchar(20) NOT NULL,
  `lab` varchar(20) NOT NULL,
  `credit_units` varchar(20) NOT NULL,
  `pre_reqs` varchar(100) NOT NULL,
  `total_hours` double NOT NULL,
  `semester` int(1) NOT NULL,
  `year_level` int(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_subjects`
--

INSERT INTO `tbl_subjects` (`subject_id`, `subject`, `descriptive`, `lec`, `lab`, `credit_units`, `pre_reqs`, `total_hours`, `semester`, `year_level`, `updated_at`, `created_at`) VALUES
(143, 'RS 1', 'Scrip. & Old Test. Salvation History', '3', '0', '3', 'None', 3, 1, 1, '2018-11-08 18:21:57', '2018-11-08 18:21:57'),
(144, 'Hist 1', 'Phil History w/Politics & Government', '3', '0', '3', 'None', 3, 1, 1, '2018-11-08 18:23:50', '2018-11-08 18:23:50'),
(145, 'IT101', 'IT Fundamentals', '2', '1', '3', 'None', 5, 1, 1, '2018-11-08 18:24:47', '2018-11-08 18:24:47'),
(146, 'Acctg 1', 'Accounting Principles', '3', '0', '3', 'Math 2', 3, 1, 4, '2018-11-08 18:28:58', '2018-11-08 18:28:58'),
(147, 'Math 1', 'College Algebra', '3', '0', '3', 'None', 3, 2, 1, '2018-11-08 18:32:23', '2018-11-08 18:32:23'),
(148, 'Algorithm 101', 'Test Description', '3', '5', '8', 'None', 11, 1, 1, '2018-11-11 01:12:03', '2018-11-11 01:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subreq`
--

CREATE TABLE `tbl_subreq` (
  `request_id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `student_email` varchar(255) NOT NULL,
  `evaluated` int(1) NOT NULL,
  `standing` int(1) NOT NULL,
  `request_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `user_type` int(1) NOT NULL,
  `course_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`username`, `password`, `name`, `user_type`, `course_id`) VALUES
('admin2018', '123456', 'Minda Min', 1, 0),
('arthurmorgan', 'waylingaw32@yahoo.com', 'Emma Jones', 1, 0),
('luiscadiz2018', '123456', 'Luis Cadiz', 2, 2),
('rhandy', '123456', 'Rhandy Oyao', 2, 1),
('theadmin', '123456', 'admin min min', 2, 17);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_countries`
--
ALTER TABLE `tbl_countries`
  ADD PRIMARY KEY (`country_code`);

--
-- Indexes for table `tbl_course`
--
ALTER TABLE `tbl_course`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `tbl_faculty`
--
ALTER TABLE `tbl_faculty`
  ADD PRIMARY KEY (`faculty_id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD UNIQUE KEY `payment_code` (`payment_code`);

--
-- Indexes for table `tbl_prereq`
--
ALTER TABLE `tbl_prereq`
  ADD PRIMARY KEY (`prereq_id`);

--
-- Indexes for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  ADD PRIMARY KEY (`schedule_id`);

--
-- Indexes for table `tbl_semester`
--
ALTER TABLE `tbl_semester`
  ADD PRIMARY KEY (`semester`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `tbl_studsubjects`
--
ALTER TABLE `tbl_studsubjects`
  ADD PRIMARY KEY (`studsubject_id`);

--
-- Indexes for table `tbl_subcourse`
--
ALTER TABLE `tbl_subcourse`
  ADD PRIMARY KEY (`subcourse_id`);

--
-- Indexes for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `tbl_subreq`
--
ALTER TABLE `tbl_subreq`
  ADD PRIMARY KEY (`request_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_course`
--
ALTER TABLE `tbl_course`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tbl_faculty`
--
ALTER TABLE `tbl_faculty`
  MODIFY `faculty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `tbl_payment`
--
ALTER TABLE `tbl_payment`
  MODIFY `payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_prereq`
--
ALTER TABLE `tbl_prereq`
  MODIFY `prereq_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_schedule`
--
ALTER TABLE `tbl_schedule`
  MODIFY `schedule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_semester`
--
ALTER TABLE `tbl_semester`
  MODIFY `semester` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_studsubjects`
--
ALTER TABLE `tbl_studsubjects`
  MODIFY `studsubject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_subcourse`
--
ALTER TABLE `tbl_subcourse`
  MODIFY `subcourse_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `tbl_subjects`
--
ALTER TABLE `tbl_subjects`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `tbl_subreq`
--
ALTER TABLE `tbl_subreq`
  MODIFY `request_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
