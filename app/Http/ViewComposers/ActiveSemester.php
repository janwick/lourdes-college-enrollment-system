<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Semester;

class ActiveSemester
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('activeSemester', $this->getActive());
    }

    protected function getActive() 
    {   
        $active = Semester::where('status',true)->first();
        return $active->description;
    }


}
