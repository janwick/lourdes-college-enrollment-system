<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\SchoolYear;

class SchoolYearComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('schoolyears', $this->getSchoolYears());
    }

    protected function getSchoolYears() 
    {   
        $schoolyears = SchoolYear::get();
        return $schoolyears;
    }


}
