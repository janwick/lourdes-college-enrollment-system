<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Semester;

class SemesterComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('view_semesters', $this->getSemesters());
    }

    protected function getSemesters() 
    {   
        $semesters = Semester::get();
        return $semesters;
    }


}
