<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\SchoolYear;

class ActiveSchoolYear
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('activeSchoolYear', $this->getActive());
    }

    protected function getActive() 
    {   
        $active = SchoolYear::where('status',true)->first();
        return $active->school_year;
    }


}
