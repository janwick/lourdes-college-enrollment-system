<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Database;
use App\Student;
use App\Course;
use App\Payment;
use App\StudentSubjects;

class CashierController extends Controller
{
    public function index() {

        $data['records'] = $this->paymentRecords();
        return view('cashier.index')->with($data);
    }

    private function paymentRecords() {
        $database = new Database;
        return $database->fetchConfirmed();
    }

    public function viewRecord() {
        $database = new Database;
        $student = Student::where('email',request()->get('email'))->first();
        $data['student'] =  $student;
        $data['course'] = Course::where('course_id',$student->course_id)->first();
        $data['schedules'] = $database->fetchConfirmedRecord(request()->get('email'));
        return view('cashier.view')->with($data);
    }

    public function viewPaymentHistory() {
        $database = new Database;
        $student = Student::where('email',request()->get('email'))->first();
        $data['student'] =  $student;
        $data['course'] = Course::where('course_id',$student->course_id)->first();
        $data['schedules'] = $database->viewStudentHistoryFromCashier(request()->get('email'));
        return view('cashier.display')->with($data);
    }

    public function confirmPayment(Request $request) {
        $studsubject_id = $request->studsubject_id;
        $amount = $request->amount;
        $miscellaneous = $request->miscellaneous;
        $email = $request->student_email;
       
        if(!$amount) {
            return redirect()->back()->with('warning', "Please complete all required subject fees");
        }
        if(!$miscellaneous) {
            return redirect()->back()->with('warning', "Please enter miscellaneous fee");
        }
        else {
            $dummy = array();
            $dummy2 = array();
            foreach($studsubject_id as $key => $value) {
                $data = array(
                    'studsubject_id' => $value,
                    'amount' => $amount[$key],
                    'miscellaneous' => $miscellaneous,
                    'created_at' => Now(),
                    'updated_at' => Now()
                );


                $dummy[] = $data;
     
                StudentSubjects::where('studsubject_id',$value)->update(['status' => 4]);
            }

            Payment::insert($dummy);
            Student::where('email',$email)->update(['enrolled' => true]);

            return redirect('account/cashier')->with('success', "Payment confirmed");
        }
    
    }

    public function paymentHistory() {
        $database = new Database;
        $data['records'] = $database->fetchHistoriesFromCashier();
        return view('cashier.history')->with($data);
    }

}
