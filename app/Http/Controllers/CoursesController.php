<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Database;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = $this->fetchCourses(request()->get('search'));
        $database = new Database();

        $count = Course::count();
        $data = [
            'title' => ($count > 0)? 'Courses ('.$count.')' : 'Courses (0)',
            'courses' => $courses,
            'total' => $count,
            'faculties' => $database->fetchFaculty(),
        ];

        return view('administrator.courses',$data);
    }

    private function fetchCourses($search)
    {
        $query = Course::query();
        $query->when($search, function($query) use ($search) {
            $query->where('course', 'like', "%$search%")
                  ->orWhere('description');
        });
        $query->orderBy('course','DESC');

        return $query->paginate(20);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    public function store_course(Request $request)
    {

        $course = new Course;
        $course->course = $request->course;
        $course->description = $request->description;
        $course->course_year = $request->course_year;
        $course->save();

        return back()->with('success','New course successfully added!');
    }

    public function update_course(Request $request)
    {
        $course = Course::findOrFail($request->course_id);
        $course->course = $request->course;
        $course->description = $request->description;
        $course->course_year = $request->course_year;
        $course->save();

        return back()->with('success','Changes saved!');
    }

    public function delete_course(Request $request)
    {
        $course = Course::findOrFail($request->course_id);

        $course->delete();

        return back()->with('success','Course deleted!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
