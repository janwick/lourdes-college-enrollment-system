<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subject;
use App\SubjectCourse;
use App\Course;
use App\Database;

class SubjectsController extends Controller
{
    public function index(Request $request)
    {
        if(session('logged_in') == true){
            if(session('type') == 1){

                $database = new Database();

                $course = $request->course;
                $semester = $request->semester;
                $year = $request->year;
                $search = $request->search;

                $data = [
                    'courses' => Course::get(),
                    'subjects' => $this->fetchSubjects($course, $semester, $year, $search),
                    'all' => Subject::get(),
                    'faculties' => $database->fetchFaculty(),
                ];

                return view('administrator.subjects')->with($data);


            }
            if(session('type') == 2){

                return redirect('user/login');
            }
            else{

                return redirect('login');
            }
        }
        else{

            return redirect('user/login');

        }
    }

    private function fetchSubjects($course, $semester, $year, $search)
    {
        $subjects = Subject::with('course.course')->when($semester, function($query) use ($semester) {
            $query->where('semester', '=', $semester);
        })
        ->when($year, function($query) use ($year) {
            $query->where('year_level', '=', $year);
        })
        ->when($search, function($query) use ($search) {
            $query->where('subject', 'like', "%$search%")
                  ->orWhere('descriptive','like', "%$search%");
        })
        ->when($course != 'all', function($query) use ($course) {
            return $query->whereHas('course', function ($query)  use ($course){
                $query->where('course_id', $course);
            });
        })
        ->orderBy('subject','ASC')
        ->paginate(32);

        return $subjects;
    }

    public function store(Request $request)
    {
        $username = session('username');
        $course_ids = array();

        if(!$request->course_id)
            return redirect('account/registrar/subjects')->with("warning","Please choose course for the subject.");

        $subject = new Subject;
        $subject->subject = $request->input('subject');
        $subject->descriptive = $request->input('descriptive');
        $subject->lec = $request->input('lec');
        $subject->lab = $request->input('lab');
        $subject->credit_units = $request->input('credit_units');
        $subject->pre_reqs = $request->input('pre_reqs');
        $subject->total_hours = $request->input('total_hours');
        $subject->semester = $request->input('semester');
        $subject->year_level = $request->input('year_level');
        $subject->save();

        foreach($request->course_id as $course_id) {
            $course_ids[] = [
                'subject_id' => $subject->subject_id,
                'course_id' => $course_id,
            ];
        }

        SubjectCourse::insert($course_ids);

        return redirect('account/registrar/subjects?course=all')->with('success',"New subject successfully added");

    }
}
