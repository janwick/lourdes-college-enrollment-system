<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Database;
use Illuminate\Validation\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Mail;
use App\Course;
use App\Subject;
use App\Student;
use App\User;

class PagesController extends Controller
{

    public function index()
    {
        $data = [
            'courses' => Course::get(),
        ];
        return view('pages.home')->with($data);
    }

    // Login Form view - Student
    public function login()
    {
       if(session('logged_in') == true){
            if(session('type') == 1){

                $username = session('username');
                return redirect('account/registrar/students');
            }
            if(session('type') == 2){

            }
            else{

               $urlname = session('urlname');
               return redirect('account/student/notifications/'.$urlname);
            }
        }
        else{

            $database = new Database();
            return view('pages.login');

        }


    }

    // Login Form view - User
    public function user_login()
    {


         if(session('logged_in') == true){
            if(session('type') == 1){

                $username = session('username');
                return redirect('account/registrar/students');
            }
            if(session('type') == 2){

            }
            else{

               $urlname = session('urlname');
               return redirect('account/student/notifications/'.$urlname);
            }
        }
        else{

            $database = new Database();
            return view('pages.login'); // return to registration page

        }




    }
    public function executeLogin(Request $request){

         $database = new Database(); // Initialize Database Model

        // Determine if student or users like registrar and coordinator
        if($request->user == 'student'){ // Student

                $email = $request->email; // Get user email
                $password = $request->password; // Get user password

                $logged = $database->loggedIn($email,$password);

                if($logged){ // Email and password is correct

                    if($logged->confirmed){ // Check if student is confirmed
                        $email = $logged->email;
                        $name = $logged->firstname . ' ' . $logged->lastname;
                        $course_id = $logged->course_id;
                        $course = $logged->course;
                        $year = $logged->year_level;
                        $urlname = $logged->firstname . $logged->lastname . Date('Y');

                        // Store user data as an array
                        $userdata = array(
                            'email' => $email,
                            'name' => $name,
                            'type' => 3,
                            'course_id' => $course_id,
                            'course' => $course,
                            'year' => $year,
                            'urlname' => $urlname,
                            'enrolled' => $logged->enrolled,
                            'logged_in' => true
                        );


                         // Store userdata array to session
                         $request->session()->put($userdata);

                         return redirect('account/student/notifications/'.$urlname);


                    }else{

                        return redirect('login')->with('error', 'Your account is not yet confirmed by the office of registrar. Please Check your email account.');
                    }




                }else{ //  Email or password is incorrect

                    return redirect('login')->with('error', 'Email or password is incorrect!');

                }

        }else{


                $username = $request->username; // Get user email
                $password = $request->password; // Get user password

                $logged = $database->loginUser($username,$password);

                if($logged){ // Username and password is correct

                    if($logged->user_type == 1){

                        $username = $logged->username;
                        $name = $logged->name;
                        $course_id = $logged->course_id;
                        $user_type = $logged->user_type;

                        // Store user data as an array
                        $userdata = array(
                            'username' => $username,
                            'name' => $name,
                            'type' => $user_type,
                            'type_long' => 'Registrar',
                            'course_id' => $course_id,
                            'logged_in' => true
                        );


                         // Store userdata array to session
                         $request->session()->put($userdata);

                         return redirect('account/registrar/students');


                    }else{

                         $username = $logged->username;
                         $name = $logged->name;
                         $course_id = $logged->course_id;
                         $user_type = $logged->user_type;
                         $course = Course::where('course_id',$course_id)->first();

                          $userdata = array(
                            'username' => $username,
                            'name' => $name,
                            'type' => $user_type,
                            'type_long' => 'Coordinator',
                            'course_id' => $course_id,
                            'course' =>  $course->course,
                            'logged_in' => true
                        );


                         // Store userdata array to session
                         $request->session()->put($userdata);

                         return redirect('account/coordinator/coordinator');
                    }


                }else{ //  Email or password is incorrect

                    return redirect('user/login')->with('error', 'Username or password is incorrect!');

                }
        }


    }

    public function runLogin( Request $request ) {

        $email = $request->email;
        $password = $request->password;

        $user = User::where('username',$email)->first();
        
        if(!$user)  return redirect('user/login')->with('error', 'Username or password is incorrect!');

        if (Hash::check($password, $user->password)) {
            return $this->userTypeFilter( $user );
        } else {
            return redirect('user/login')->with('error', 'Username or password is incorrect!');
        }
    
    }

    private function userTypeFilter( $user ) {
        if( $user->user_type === 4 ) {
            return $this->saveStudentSession( $user );
        } else if( $user->user_type === 3 ) {
            return $this->saveCashierSession( $user );
        } else if( $user->user_type === 2 ) {
            return $this->saveCoordinatorSession( $user );
        } else if( $user->user_type === 1 ) {
            return $this->saveAdminSession( $user );
        }
    }

    private function saveCashierSession( $user ) {

        $username = $user->username;
        $name = $user->name;
        $user_type = $user->user_type;

         $userdata = array(
           'username' => $username,
           'name' => $name,
           'type' => $user_type,
           'type_long' => 'Cashier',
           'logged_in' => true
       );


        session($userdata);

        return redirect('account/cashier');

    }

    private function saveStudentSession( $user ) {

        $database = new Database();

        $student = Student::where('email', $user->username)->first();

        if($student->confirmed == 0) {
            return back()->with('warning','Account not yet confirmed!');
        }

        $email = $student->email;
        $name = $student->firstname . ' ' . $student->lastname;
        $course_id = $student->course_id;
        $course = $student->course;
        $year = $student->year_level;
        $urlname = $student->firstname . $student->lastname . Date('Y');

        $course = Course::findOrFail($course_id);

        // Store student data as an array
        $session_data = array(
            'email' => $email,
            'name' => $name,
            'type' => 4,
            'course_id' => $course_id,
            'course' => $course->course,
            'year' => $year,
            'urlname' => $urlname,
            'enrolled' => $student->enrolled,
            'logged_in' => true
        );


        // Save student to session
       session($session_data);

       

       return redirect('account/student/notifications/'.$urlname);

    }

    private function saveCoordinatorSession( $user ) {

        $username = $user->username;
        $name = $user->name;
        $course_id = $user->course_id;
        $user_type = $user->user_type;
        $course = Course::where('course_id',$course_id)->first();

         $userdata = array(
           'username' => $username,
           'name' => $name,
           'type' => $user_type,
           'type_long' => 'Coordinator',
           'course_id' => $course_id,
           'course' =>  $course->course,
           'logged_in' => true
       );


        session($userdata);

        return redirect('account/coordinator/coordinator');

    }

    private function saveAdminSession( $user ) {

        $username = $user->username;
        $name = $user->name;
        $course_id = $user->course_id;
        $user_type = $user->user_type;

        // Store user data as an array
        $userdata = array(
            'username' => $username,
            'name' => $name,
            'type' => $user_type,
            'type_long' => 'Registrar',
            'course_id' => $course_id,
            'logged_in' => true
        );


         // Store userdata array to session
         session($userdata);

         return redirect('account/registrar/students');
    }

    // Registration Form view
    public function registration_form()
    {
        $database = new Database();
        $data['countries'] = $database->fetchCountries(); // Country list
        $data['courses'] = $database->fetchCourses(); // Course list
        return view('pages.register',$data); // return to registration page
    }

    // Submit registration form
    public function submit_form(Request $request){

    	$database = new Database();


        $fileNameToStore = 'noimage.jpg'; // Default student image


        // Get date enrolled
        $token = str_random(5);
        $year_enrolled = Date('Y');
        $raw = $request->input('firstname') .$request->input('lastname').$token.$year_enrolled;
        $date_enrolled = Date('Y-m-d');

        $date_of_birth = Date('Y-m-d',strtotime($request->input('date_of_birth')));
        // Store form inputs to array
       
        $studentData = new Student;
        $studentData->lastname = $request->input('lastname');
        $studentData->firstname = $request->input('firstname');
        $studentData->middlename = $request->input('middlename');
        $studentData->course_id = $request->input('course_id');
        $studentData->year_level = 1;
        $studentData->gender = $request->input('gender');
        $studentData->religion = $request->input('religion');
        $studentData->nationality = $request->input('nationality');
        $studentData->date_of_birth = $date_of_birth;
        $studentData->place_of_birth = $request->input('place_of_birth');
        $studentData->civil_status = $request->input('civil_status');
        $studentData->email = $request->input('email');
        $studentData->home_address = $request->input('home_address');
        $studentData->city = $request->input('city');
        $studentData->province = $request->input('province');
        $studentData->country_code = $request->input('country_code');
        $studentData->zipcode = $request->input('zipcode');
        $studentData->guardian = $request->input('guardian');
        $studentData->guardian_relationship = $request->input('guardian_relationship');
        $studentData->basic_education = $request->input('basic_education');
        $studentData->secondary_education = $request->input('secondary_education');
        $studentData->college_education = $request->input('college_education');
        $studentData->phone = $request->input('phone');
        $studentData->img = $fileNameToStore;
        $studentData->password = $request->password;
        $studentData->confirmed = 0;
        $studentData->enrolled = 0;
        $studentData->date_enrolled = $date_enrolled;

        $email = $request->input('email');

        if($request->input('email') !== $request->input('confirm_email')) { 
            return redirect('registration')->with('error', 'Email typed mismatch, please confirm email');
        }

        if($request->input('password') !== $request->input('confirm_password')) { 
            return redirect('registration')->with('error', 'Password typed mismatch, please confirm password');
        }

        $existed = $database->checkIfExist($email);



    	if($existed){

    		return redirect('registration')->with('error', "Email address already exist! It looks like you've already created an account.");

    	}else{
            

            if($studentData->save()){
                $this->saveToUserTable($studentData);

                return redirect('registration')->with('success', 'Registration successful, The office of registrar of Lourdes College will review the information you submitted and will send you an email confirmation with password to be able to login and proceed to the enrollment. Thank you!');

            }else{

                 return redirect('registration')->with('error', 'Registration unsuccessful, please try again later.');
            }
            

    	}


    }


    private function saveToUserTable( $studentData ) {


        $newUser = new User;
        $name = $studentData->firstname . ' ' . $studentData->lastname;

        $newUser->username = $studentData->email;
        $newUser->password = Hash::make($studentData->password); // Hash the password
        $newUser->name = $name;
        $newUser->user_type = 4;
        $newUser->course_id = $studentData->course_id;
        $newUser->save();


    }


    // User has confirm from email account
    public function confirmation($email){

        $database = new Database();

        $data = array(
            'confirmed' => 1
        );
        $updated = $database->confirmStudent($email,$data);

        if($updated){
              return redirect('login')->with('success', 'Registration Confirmation Successful, you can now proceed');
        }else{
              return redirect('login')->with('error', 'Registration Confirmation Failed, please try again later!');
        }

    }

    // Log the user out to the site
    public function logout(Request $request){

        $userdata = array('email','name,','type','course_id','course','year','urlname','type_long','logged_in','img','username','enrolled');

        $request->session()->forget($userdata);
        return redirect('login');
    }

    // Subjects in each course
   public function subjects(Request $request, $course){
       

         if(session('logged_in') == true){
            if(session('type') == 1){

               return redirect('user/login');

            }
            if(session('type') == 2){

                return redirect('user/login');
            }
            if(session('type') == 3) {
                $database = new Database();

                //dump($course);
    
                $semester = $request->semester;
                $year = $request->year;
                $search = $request->search;
                $data['course'] = $course;
                $data['subjects'] = $this->fetchSubjects($semester,$year,$search,$course); // Course list
                return view('pages.subjects',$data);
            }
            else{

               return redirect('login');
            }
        }
        else{

            return redirect('login');

        }
   }


    private function fetchSubjects($semester,$year,$search,$course)
    {
        $subjects = Subject::with('course.course')->when($semester, function($query) use ($semester) {
            $query->where('semester', '=', $semester);
        })
        ->when($year, function($query) use ($year) {
            $query->where('year_level', '=', $year);
        })
        ->when($search, function($query) use ($search) {
            $query->where('subject', 'like', "%$search%")
                  ->orWhere('descriptive','like', "%$search%");
        })
        ->when($course != 'all', function($query) use ($course) {
            return $query->whereHas('course', function ($query)  use ($course){
                $query->whereHas('course', function ($query)  use ($course){
                    $query->where('course', $course);
                });
            });
        })
        ->orderBy('subject','ASC')
        ->paginate(32);


        return $subjects;
    }


}
