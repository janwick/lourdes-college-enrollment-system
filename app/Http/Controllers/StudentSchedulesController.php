<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use App\Student;
use App\Semester;
use App\Subject;
use App\Database;
use App\StudentSubjects;

class StudentSchedulesController extends Controller
{
    public function temporaries(Request $request) {
        if(session('logged_in') == true){
            if(session('type') == 1){

               return redirect('user/login');

            }
            if(session('type') == 2){

                return redirect('user/login');
            }
            else{

                $database = new Database();
                $email = session('email');
                $data['schedules'] = $database->fetchTemps($email);
                return view('student.selected',$data);
            }
        }
        else{

            return redirect('user/login');

        }
    }

    public function storeTemporaries(Request $request) {

        $schedule_id = $request->schedule_id;
        $email = session('email');
        $holder = array();

        foreach($schedule_id as $sched_id){

            $data = array(
                'schedule_id' => $sched_id,
                'student_email' => $email,
                'status' => 0,
                'created_at' => now(),
                'updated_at' => now(),
            );

            $holder[] = $data;

        }
        
        StudentSubjects::insert($holder);

        return redirect()->back()->with('success', 'Selected schedules added to your temporary schedules list.');
    }

    public function tempEvents(Request $request){

        $studsubject_id = $request->studsubject_id;
        $database = new Database();
        $email = session('email');
        $course = session('course');
        $urlname = session('urlname');

        $requested = $database->fetchTemps($email);

        if($request->has('enroll')) {
            $this->enroll($studsubject_id);
            return redirect('account/student/temps/'.$urlname)->with('success','Selected schedules sent to coordinator for evaluation');
        } else {
            foreach($studsubject_id as $id){
                $removed = $database->removeTemps($id);
            }
            return redirect('account/student/temps/'.$urlname);
        }

    }

    private function enroll($studsubject_id) {
        $database = new Database();
        $coordinator = $database->getCoordinator(session('course_id'));
        $student = Student::where('email',session('email'))->first();
        foreach($studsubject_id as $id){
            StudentSubjects::where('studsubject_id',$id)->update(['status' => 1]);
        }
        $count = count($studsubject_id);

        $data = array(
             'notification' => $student->lastname. ' submitted '.$count.' subjects for evaluation',
             'sent_from' => session('email'),
             'sent_to' => $coordinator->username,
             'type' => 2,
             'status' => 0,
             'date_sent' => Date('Y-m-d H:i:s')
        );
        $inserted = $database->insertNotification($data);
    }

    // private function checkConflicts($schedule_id, student_email) {
    //   $hasConflict = Schedule::
    // }
}
