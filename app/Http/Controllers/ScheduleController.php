<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Schedule;
use App\Semester;
use App\Subject;
use App\Database;

class ScheduleController extends Controller
{

    public function index(Request $request)
    {
        if(session('logged_in') == true){
            if(session('type') == 1){

                $database = new Database();

                $schedule_day = $request->schedule_day;
                $semester = $request->semester;
                $school_year = $request->school_year;
                $search = $request->search;
                $sy = Schedule::select('school_year')->distinct()->get();

                $data = [
                    'faculties' => $database->fetchFaculty(),
                    'schedules' =>$this->fetchSchedules( $schedule_day,$semester, $school_year,$search ),
                    'results' => $search,
                    'subjects' => Subject::get(),
                    'sy' => $sy,
                ];

                return view('administrator.schedules')->with($data);

            }
            if(session('type') == 2){

                return redirect('user/login');
            }
            else{

                return redirect('login');
            }
        }
        else{

            return redirect('user/login');

        }
    }

    private function fetchSchedules( $schedule_day,$semester, $school_year,$search )
    {
        $schedules = Schedule::with('subject','faculty')
                    ->where('tbl_schedule.schedule_day',$schedule_day)
                    ->where('tbl_schedule.semester',$semester)
                    ->where('tbl_schedule.school_year',$school_year)
                    ->orderBy('tbl_schedule.start_time', 'ASC')
                    ->paginate(20);

        return $schedules;
    }

    public function store(Request $request)
    {
        $first = Date('Y');
        $second = Date('Y',strtotime('+1 Year'));
        $school_year = $first.'-'.$second;
        $semester = Semester::where('status',1)->first();

        $schedule = new Schedule;

        if($this->hasConflict( $request->input('schedule_day'), $request->input('start_time'), $semester->semester, $school_year, $request->input('room') ))
            return back()->with("warning","Schedule has conflict!");

        $schedule->schedule_day = $request->input('schedule_day');
        $schedule->school_year = $school_year;
        $schedule->start_time = $request->input('start_time');
        $schedule->end_time = $request->input('end_time');
        $schedule->semester = $semester->semester;
        $schedule->subject_id = $request->input('subject_id');
        $schedule->room = $request->input('room');
        $schedule->slots = $request->input('slots');
        $schedule->status = 1;
        $schedule->faculty_id = $request->input('faculty_id');
        $schedule->save();

        return back()->with('success',"New schedule successfully added!");

    }

    private function hasConflict( $day, $start_time, $semester, $school_year, $room )
    {
        $schedule = Schedule::where('schedule_day', '=', $day)
                ->where('start_time', '=', $start_time)
                ->where('semester', '=', $semester)
                ->where('school_year', '=', $school_year)
                ->where('room', '=', $room)
                ->first();

        return ($schedule)? true : false;
    }

}
