<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
         View::composer('*', 'App\Http\ViewComposers\SemesterComposer');
         View::composer('*', 'App\Http\ViewComposers\SchoolYearComposer');
         View::composer('*', 'App\Http\ViewComposers\ActiveSemester');
         View::composer('*', 'App\Http\ViewComposers\ActiveSchoolYear');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
