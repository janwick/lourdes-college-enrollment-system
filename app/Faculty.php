<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    protected $table = 'tbl_faculty';
    protected $primaryKey = 'faculty_id';

    public function schedule(){
        return $this->hasMany('App\Schedule','faculty_id');
    }
}
