<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TempSubjects extends Model
{
    protected $table = 'tbl_tempsubjects';
}
