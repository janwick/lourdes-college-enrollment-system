<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSubjects extends Model
{
    protected $table = 'tbl_studsubjects';
}
