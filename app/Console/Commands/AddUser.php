<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use App\User;

class AddUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:add {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->argument('email');
        $name = $this->ask('What is your name?');
        if ($this->confirm('Let system generate password for you?')) {
            $password = str_random(10);
            $this->info("Your password: $password");
        } else {
            $password = $this->secret('Please enter your new password');
        }
        $password = Hash::make($password);

        $data = [
            'username' => $username,
            'name' => $name,
            'password' => $password,
        ];

        $this->saveUser( $data );
    }

    private function saveUser( $data ) {
        $user = new User;
        $user->username = $data['username'];
        $user->name = $data['name'];
        $user->password = $data['password'];
        $user->user_type = 1;
        $user->course_id = 0;
        $user->save();
    }
}
