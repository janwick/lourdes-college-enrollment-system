<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = 'tbl_subjects';
    protected $primaryKey = 'subject_id';

    public function course(){
        return $this->hasMany('App\SubjectCourse','subject_id','subject_id');
    }

    public function schedule(){
        return $this->hasMany('App\Schedule','subject_id');
    }

}
