<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'tbl_schedule';
    protected $primaryKey = 'schedule_id';

    public function subject(){
        return $this->belongsTo('App\Subject','subject_id');
    }

    public function faculty(){
        return $this->belongsTo('App\Faculty','faculty_id');
    }
}
