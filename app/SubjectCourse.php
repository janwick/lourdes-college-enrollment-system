<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectCourse extends Model
{
    protected $table = 'tbl_subcourse';
    protected $primaryKey = 'subcourse_id';

    public function subject(){
        return $this->belongsTo('App\Subject','subject_id','subject_id');
    }

    public function course(){
        return $this->belongsTo('App\Course','course_id','course_id');
    }
}
