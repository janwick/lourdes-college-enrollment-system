@if( session('success'))
<script>
    toastr.success('<?php echo Session::get('success');?>')
</script>
@elseif( session('info') )
<script>
    toastr.info('<?php echo Session::get('info');?>')
</script>
@elseif(session('error') )
<script>
    toastr.error('<?php echo Session::get('error');?>')
</script>
@elseif( session('warning') )
<script>
    toastr.warning('<?php echo Session::get('warning');?>')
</script>
@endif
   
