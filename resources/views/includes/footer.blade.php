<footer class="page-footer black">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">About</h5>
          <p class="pink-text">Lourdes College BUSAC-IT Online Enrollment System</p>


        </div>
        <div class="col s12">
            <hr>
        </div>
        <div class="col s12">
          <h5 class="white-text">Courses Offered - Quick Links</h5>
          <div class="row">
            @foreach($courses as $course)
              <div class="col s4">
                <p><a href="{{url('courses')}}/{{$course->course}}?semester=1&year=1" class="waves-effect waves-teal">{{ $course->description }}</a></p>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <div class="footer-copyright center darken">
      <div class="container"> &copy {{Date('Y')}} Lourdes College Higher Education @if(session('logged_in')) <a class="blue-text" href="{{url('logout')}}">Logout</a> @endif
      </div>
    </div>
  </footer>
