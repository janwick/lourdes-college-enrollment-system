@extends('layouts.app')


@section('content')
<br>
<div class="row profile">
	@include('includes.toasts')
	<div class="col s3">
		@include('student.sidenav')
	</div>

	<div class="col s9">
        <h5>Payment Histories</h5>
        <hr>
        @if(count($records) > 0)
			<div class="collection">
				@foreach($records as $record)
					<a href="{{url('account/cashier/viewPaymentHistory')}}?email={{$record->student_email}}" class="collection-item">{{ $record->lastname }} {{ $record->firstname }} ( Click to view )</a>
				@endforeach
			</div>
			@else 
				<hr>
				<h4><center>No records yet </center></h4>
				<hr>
			@endif
    </div>

</div>
	
@endsection