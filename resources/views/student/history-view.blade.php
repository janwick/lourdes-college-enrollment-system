@extends('layouts.app')


@section('content')
<br>
<div class="row profile">
	@include('includes.toasts');
	<div class="col s3">
		@include('student.sidenav')
	</div>

	<div class="col s9">

    <div class="card attached" style="padding-bottom:30px;" id="printJS-form">
                        <div class="ui message attached">
                              <div class="content">
                                <div class="header">
                                  <i class="fa fa-exchange" aria-hidden="true"></i> Student Assessment
                                </div>
                              </div>
                            </div>
                        <div class="card-content">
								<h5>Name: {{ $student->lastname }} {{ $student->firstname }} </h5> 
								<h5>Course and year: {{ $course->course }} - {{ $student->year_level }} </h5>
                                @if(count($schedules) > 0)
                                    <table class="ui celled padded table attached" style="margin-bottom: 2em;">
                                      <thead>
                                          <th>Schedule Day</th>
                                          <th>Time</th>
                                          <th>Subject</th>
                                          <th>Room</th>
										  <th>Amount</th>
                                      </thead>
                                  
                                      <tbody>
                                        @php 
                                            $total = 0;
                                        @endphp
                                         @foreach($schedules as $schedule)
                                            @php $total += $schedule->amount @endphp
										 		<input type="hidden" value="{{$schedule->studsubject_id}}" name="studsubject_id[]">
                                               <tr>
                                                 <td>{{$schedule->schedule_day}}</td>
                                                 <td>{{Date('g:i A', strtotime($schedule->start_time))}} - {{Date('g:i A', strtotime($schedule->end_time))}}</td>
                                                 <td>{{$schedule->subject}}</td>
                                                 <td>{{$schedule->room}}</td>
												 <td>
                                                    {{$schedule->amount}}
												 </td>
                                              </tr>
                                          @endforeach
                                          @php $total += $schedule->miscellaneous @endphp
                                      </tbody>
                                    </table>

                                    <h6>Miscellaneous Fees: {{ $schedule->miscellaneous }}</h6>

									<div class="row" style="margin-top: 20px margin-bottom: 20px;">
										<div class="col s6">
											<h3>Total Fees: <span class="totalFees">{{ $total }}</span> </h3>
										</div>
									</div>

						
									<button type="button" class="waves-effect waves-light btn green" style="float:right;" onclick="printJS('printJS-form', 'html')" >Print</button>
                              @else
                                  <div class="ui message warning">
                                  <div class="content">
                                    <div class="header">
                                      No Temporary schedules yet
                                    </div>
                                  </div>
                                </div>
                              @endif
                        </div>
                    </div>
	

	</div>

</div>
	
@endsection