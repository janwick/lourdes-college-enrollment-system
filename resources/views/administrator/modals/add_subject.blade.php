 <!-- View unverified student information -->
 <div id="addSubject" class="modal modal-fixed-footer">
 {!! Form::open(['action' => 'SubjectsController@store', 'method' => 'POST']) !!}
    <div class="modal-content">
      <h5 class="center"><span class="fa fa-book"></span> Add subject</h5>
      <br>
      <div class="ui attached info message">
          <p><span class="fa fa-info-circle"></span> Total hours / Week will automatically calculated based on number of lectures, laboratory.</p>
        </div>

        <div class="row">
            <div class="input-field col s6">
              <input  type="text" class="validate" name="subject" required>
              <label for="subject">Subject</label>
            </div>

             <div class="input-field col s6">
              <input  type="text" class="validate" name="descriptive" required>
              <label for="descriptive">Descriptive Title</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s6">
              <input  type="number" class="validate lec" name="lec" required>
              <label for="lec">Lecture</label>
            </div>

             <div class="input-field col s6">
              <input  type="number" class="validate lab" name="lab" required >
              <label for="lab">Laboratory</label>
            </div>
        </div>

         <div class="row">
            <div class="col s6">
            <label for="credit_units">Credit Units</label>
              <input  type="number" class="validate credit_units" name="credit_units" required readonly>
            </div>

             <div class="col s6">
              <label for="total_hours">Total Hours / Week</label>
              <input type="number" class="validate total_hours" readonly name="total_hours" required readonly>
            </div>
        </div>

        <div class="row">
            <div class="col s6">
             <label for="pre_req">Pre requisites</label>
              <input  type="text" class="validate" id="pre_reqs" name="pre_reqs" required>

            </div>

              <div class="input-field col s6">
               <select name="course_id[]" required multiple>
                  <option value="" disabled selected>Select course</option>
                   @foreach($courses as $course)
                        <option value="{{$course->course_id}}">{{$course->course}}</option>
                   @endforeach
                </select>
              <label for="course_id">Subject course</label>
            </div>
        </div>

         <div class="row">


             <div class="input-field col s6">
               <select name="semester" required>
                  <option value="" disabled selected>Select semester</option>
                  <option value="1">1st semester</option>
                  <option value="2">2nd semester</option>
                  <option value="3">Summer</option>
                </select>
              <label for="semester">Semester</label>
            </div>

              <div class="input-field col s6">
               <select name="year_level" required>
                  <option value="" disabled selected>Select year</option>
                  <option value="1">First year</option>
                  <option value="2">Second year</option>
                  <option value="3">Third year</option>
                  <option value="4">Fourth year</option>
                </select>
              <label for="year_level">Year level</label>
            </div>

        </div>

         <div class="row">


             <div class="input-field col s6">

            </div>

        </div>


    </div>
    <div class="modal-footer">
         <button type="submit" class="modal-action modal-close waves-effect waves-green btn green">Add subject</button>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn red ">Close</a>
    </div>

    {!! Form::close() !!}
 </div>
