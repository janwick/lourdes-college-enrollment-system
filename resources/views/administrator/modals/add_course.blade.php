<!-- View unverified student information -->
 <div id="addCourse" class="modal modal-fixed-footer">
 {!! Form::open(['action' => 'CoursesController@store_course', 'method' => 'POST']) !!}
    <div class="modal-content">
      <h5 class="center"><span class="fa fa-book"></span> Add Course</h5>
      <br>
        <div class="row">
            <div class="input-field col s12">
              <input  type="text" class="validate" name="course" required>
              <label for="subject">Course</label>
            </div>

             <div class="input-field col s12">
              <input  type="text" class="validate" name="description" required>
              <label for="descriptive">Course Description</label>
            </div>

            <div class="input-field col s12">
              <input  type="number" class="validate" name="course_year" required>
              <label for="course_year">Year</label>
            </div>
        </div>


    </div>
    <div class="modal-footer">
         <button type="submit" class="modal-action modal-close waves-effect waves-green btn green">Add Course</button>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn red ">Close</a>
    </div>

    {!! Form::close() !!}
 </div>
