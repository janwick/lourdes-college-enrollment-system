<div id="addSchedule" class="modal modal-fixed-footer">
 {!! Form::open(['action' => 'ScheduleController@store', 'method' => 'POST']) !!}
    <div class="modal-content">
      <h5 class="center" id="sched-title"> Add Schedule</h5>
      <br>
      <input  id="schedule_id" name="schedule_id" type="hidden" class="validate">
      <div class="ui attached info message">
          <p><span class="fa fa-info-circle"></span> Schedule will be added in current school year and semester.</p>
        </div>
    <br>

        <div class="row">
            <div class="col s6">
            <label for="schedule_day">Schedule day:</label>
             <select name="schedule_day" id="schedule_day" required>
                  <option value="MTH">Monday - Thursday</option>
                  <option value="TF">Tuesday - Friday</option>
                  <option value="WED">Wednesday</option>
                  <option value="SAT">Saturday</option>
                </select>
            </div>

            <div class="col s6">
              <label for="start_time">Start time:</label>
              <input id="start_time" type="time" class="validate" name="start_time" required>
            </div>
        </div>

        <div class="row">
             <div class="col s6">
              <label for="end_time">End time:</label>
              <input id="end_time" type="time" class="validate" name="end_time" required>
            </div>

             <div class="col s6">
              <label for="room">Room:</label>
              <input  id="room" type="text" class="validate" name="room" required>
            </div>
        </div>

         <div class="row">

            <div class="col s6">
             <label for="course_id">Assign faculty</label>
              <select name="faculty_id" id="faculty_id" required>
                   @foreach($faculties as $faculty)
                        <option value="{{$faculty->faculty_id}}">{{$faculty->faculty_name}}</option>
                   @endforeach
                </select>

            </div>

             <div class="col s6">
             <label for="subject_id">Subject</label>
              <select name="subject_id" id="subject_id" required>
                   @foreach($subjects as $subject)
                        <option value="{{$subject->subject_id}}">{{$subject->subject}}</option>
                   @endforeach
                </select>

            </div>
        </div>

         <div class="row">

           <div class="col s6">
                <label for="slots">Available slots:</label>
                <input id="slots" type="number" class="validate" name="slots" required>
            </div>


        </div>


    </div>
    <div class="modal-footer">
         <button type="submit" class="modal-action modal-close waves-effect waves-green btn green">Save schedule</button>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn red ">Close</a>
    </div>

    {!! Form::close() !!}
 </div>
