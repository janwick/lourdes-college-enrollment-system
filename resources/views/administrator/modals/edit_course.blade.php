<!-- View unverified student information -->
 <div id="editCourse" class="modal modal-fixed-footer">
 {!! Form::open(['action' => 'CoursesController@update_course', 'method' => 'POST']) !!}
    <div class="modal-content">
      <h5 class="center"><span class="fa fa-book"></span> Add Course</h5>
      <br>
        <div class="row">
           <input  type="hidden" class="validate" name="course_id" id="c_id" required>
            <div class="input-field col s12">
              <input  type="text" class="validate" name="course" required id="course" autofocus>
              <label for="description">Course</label>
            </div>

            <div class="input-field col s12">
              <input  type="text" class="validate" name="description" required id="description" autofocus>
              <label for="description">Course Description</label>
            </div>

            <div class="input-field col s12">
              <input  type="number" class="validate" name="course_year" required id="course_year" autofocus>
              <label for="course_year">Year</label>
            </div>
        </div>

    </div>
    <div class="modal-footer">
         <button type="submit" class="modal-action modal-close waves-effect waves-green btn green">Save Changes</button>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn red ">Close</a>
    </div>

    {!! Form::close() !!}
 </div>
