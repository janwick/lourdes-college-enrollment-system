@extends('layouts.app')
@section('content')

<br>
<div class="row profile">

  <div class="col s3">
    @include('administrator.sidenav')
  </div>

  <div class="col s9">
    <div class="card">

      <div class="ui attached message">
        <div class="header pink-text">
          @if(count($payments) > 0)
           <i class="fa fa-book" aria-hidden="true"></i> Payments ({{count($payments)}})
        @else
          <i class="fa fa-book" aria-hidden="true"></i> Payments (0)
        @endif

        </div>
      </div>

      <div class="card-content">
        <div class="row">
          <div class="col s6">

          </div>

        </div>

        <div class="row">
              <div class="col s3">
                   <input placeholder="Payment Code" id="search" type="text" class="validate">
              </div>
        </div>

        @if(count($payments) > 0)


              @include('includes.message')
              <div class="ui warning message onpaid">
                  <div class="header">
                    Are you sure you want to perform action?
                  </div>
                {!! Form::open(['route' => 'enrollment.paid', 'method' => 'POST']) !!}
                 <div class="ui form">
                  <input type="hidden" name="payment_id" id="payment_id">
                    <button type="submit" class="waves-effect waves-light btn green">Yes</button> <a href="{{route('payments.index')}}" class="waves-effect waves-light btn red cancel">No</a>
                  </div>
                  {!! Form::close() !!}
              </div>

               <div class="ui warning message onremove">
                  <div class="header">
                    Are you sure you want to perform action?
                  </div>
                {!! Form::open(['route' => 'enrollment.paid', 'method' => 'POST']) !!}
                 <div class="ui form">
                  <input type="hidden" name="payment_id" id="payment_id">
                    <button type="submit" name="rejected" class="waves-effect waves-light btn green">Yes</button> <a href="{{route('payments.index')}}" class="waves-effect waves-light btn red cancel">No</a>
                  </div>
                  {!! Form::close() !!}
              </div>
                            <table class="ui celled padded table attached">
                              <thead>
                                  <th> Payment Code </th>
                                 {{--  <th> Enrollment Requested By</th> --}}
                                  <th> Payment Status </th>
                                  <th> Issued Date </th>
                                  <th class="text-center"> Options </th>
                              </thead>

                              <tbody>
                                  @foreach($payments as $payment)
                                   <tr>
                                     <td>{{$payment->payment_code}}</td>
                                    {{--  <td>{{$payment->request_id}}</td> --}}
                                     <td>@if($payment->payment_status) <strong class="green-text">PAID</strong>@else <strong class="red-text">NOT PAID</strong>@endif</td>
                                     <td>{{Date('m-d-Y',strtotime($payment->created_at))}}</td>
                                     <td class="text-center">
                                        <button data-activates="options" class="btn-flat pink-text dropdown-button payment"
                                        data-payment_id="{{$payment->payment_id}}"
                                        ><i class="material-icons">more_vert</i></button>

                                     </td>

                                  </tr>

                                  @endforeach
                              </tbody>
                            </table>
                          <div class="row center">
                      {{ $payments->links() }}
                  </div>
                      @else
                           <div class="row center">
                      <h5><span class="fa fa-search"></span> No payments record yet</h5>
                  </div>
                      @endif
      </div>
    </div>
  </div>

</div>

  <!-- Subjects options -->
  <ul id='options' class='dropdown-content'>
    <li><a href='#!' class="green-text center modal-trigger paid">Paid</a></li>
    <li class="divider"></li>
    <li><a href='#!' class="red-text center modal-trigger delete">Cancel</a></li>
    <li class="divider"></li>
  </ul>


<script>
   $(document).ready(function() {

    var url  = window.location.href;

    var c = localStorage.getItem('course');
    var sem = localStorage.getItem('semester');
    var y = localStorage.getItem('year')
    var url = window.location.href;

    $('.onpaid').hide();
    $('.onremove').hide();


    if(c != null){
      $( "#course" ).val(c)

    }
    else{
      $( "#course" ).val('0')
    }

    if(sem != null){
      $( "#semester" ).val(sem)


    }
    else{
      $( "#semester" ).val('0')
    }

    if(y != null){
      $( "#year" ).val(y)

    }
    else{
      $( "#year" ).val('0')
    }



    $('.delete').click(function(){
      $('.onremove').show();
    });

      $('.paid').click(function(){
       $('.onpaid').show();
    });


    $(".payment").click(function() {

      var id = $(this).data("payment_id");

      $("#payment_id").val(id);


    });


     $('.modal').modal();
       $('select').material_select();


      $(".lab").on("change paste keyup", function() {

            let total_hours = 0;
            let lec = $(".lec").val();
            let credit_units = 0;

            let lab = $(this).val();

            if(lab >= 1){
              credit_units = parseInt(lec) + parseInt(lab);
              total_hours = parseInt(lec) + parseInt(credit_units);
              $(".credit_units").val(credit_units);
              $(".total_hours").val(total_hours);

            }else{
              $(".total_hours").val(parseInt(lec));
              $(".credit_units").val(parseInt(lec));

            }
           console.log(lab);

        });

        $("#lab").on("change paste keyup", function() {

            let total_hours = 0;
            let lec = $("#lec").val();
            let credit_units = 0;

            let lab = $(this).val();

            if(lab >= 1){
              credit_units = parseInt(lec) + parseInt(lab);
              total_hours = parseInt(lec) + parseInt(credit_units);
              $("#credit_units").val(credit_units);
              $("#total_hours").val(total_hours);

            }else{
              $("#total_hours").val(parseInt(lec));
              $("#credit_units").val(parseInt(lec));

            }
           console.log(lab);

        });



        $( "#course" ).change(function() {

           var course = $(this).val();
           localStorage.setItem('course',course);
           window.location.href = updateQueryStringParameter( url, 'course', course )

        });


        $( "#semester" ).change(function() {

           var semester = $(this).val();
           localStorage.setItem('semester',semester);
           window.location.href = updateQueryStringParameter( url, 'semester', semester )

        });


         $( "#year" ).change(function() {

           var year = $(this).val();
           localStorage.setItem('year',year);
           window.location.href = updateQueryStringParameter( url, 'year', year )

        });

         $('#search').keypress(function (e) {
      if (e.which == 13) {
         window.location.href = updateQueryStringParameter( url, 'search', $(this).val() )
      }
    });


        function updateQueryStringParameter(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
        if( value === undefined ) {
          if (uri.match(re)) {
          return uri.replace(re, '$1$2');
        } else {
          return uri;
        }
        } else {
          if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        } else {
          var hash =  '';
          if( uri.indexOf('#') !== -1 ){
              hash = uri.replace(/.*#/, '#');
              uri = uri.replace(/#.*/, '');
          }
          var separator = uri.indexOf('?') !== -1 ? "&" : "?";
          return uri + separator + key + "=" + value + hash;
        }
        }
    }

    });
</script>
@endsection
