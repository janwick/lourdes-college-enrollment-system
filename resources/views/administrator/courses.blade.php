@extends('layouts.app')
@section('content')

<br>
<div class="row profile">

    <div class="col s3">
        @include('administrator.sidenav')
    </div>

    <div class="col s9">
        <div class="card">

            <div class="ui attached message">
              <div class="header pink-text">
                <i class="fa fa-list" aria-hidden="true"></i> {{ $title }}
              </div>
            </div>

            <div class="card-content">
                <div class="row">
                    <div class="col s6">

                    </div>

                    <div class="col s6 right">
                         <a href='#addCourse' class="btn-floating btn-large waves-effect waves-light pink right modal-trigger"><i class="material-icons">add</i></a>
                    </div>
                </div>

                <div class="row">
                      <div class="col s6">
                           <input placeholder="Search" id="search" type="text" class="validate">
                      </div>
                </div>

                @if($total > 0)

                    <div class="ui warning message onremove">
                      <div class="header">
                        Are you sure you want to remove course?
                      </div>
                    {!! Form::open(['action' => 'CoursesController@delete_course', 'method' => 'POST']) !!}
                     <div class="ui form">
                      <input type="hidden" name="course_id" id="courseid">
                        <button type="submit" name="rejected" class="waves-effect waves-light btn green">Yes</button> <a href="{{url('account/registrar/subjects')}}" class="waves-effect waves-light btn red cancel">No</a>
                      </div>
                      {!! Form::close() !!}
                  </div>

                            @include('includes.message')
                            <table class="ui celled padded table attached">
                              <thead>
                                  <th> ID </th>
                                  <th> Course </th>
                                  <th> Description </th>
                                   <th> Year </th>
                                  <th class="text-center"> Options </th>
                              </thead>

                              <tbody>
                                  @foreach($courses as $course)
                                   <tr>
                                     <td>{{$course->course_id}}</td>
                                     <td>{{$course->course}}</td>
                                     <td>{{$course->description}}</td>
                                     <td>{{$course->course_year}}</td>
                                     <td class="text-center">
                                        <button data-activates="options" class="btn-flat pink-text dropdown-button course-option"
                                        data-id="{{$course->course_id}}"
                                        data-course="{{$course->course}}"
                                        data-description="{{$course->description}}"
                                        data-year="{{$course->course_year}}"
                                        ><i class="material-icons">more_vert</i></button>

                                     </td>

                                  </tr>

                                  @endforeach
                              </tbody>
                            </table>
                            <div class="row center">
                                    {{ $courses->appends(request()->input())->links() }}
                            </div>
                      @else
                           <div class="row center">
                                    <h5><span class="fa fa-search"></span> No subjects found.</h5>
                            </div>
                      @endif
            </div>
        </div>
    </div>

</div>

{{-- Modals --}}
@include('administrator.modals.add_course')
@include('administrator.modals.edit_course')

  <!-- Subjects options -->
  <ul id='options' class='dropdown-content'>
    <li><a href="#editCourse" class="blue-text center modal-trigger"><span class="fa fa-pencil"></span></a></li>
    <li class="divider"></li>
    <li><a href="#" class="red-text center delete"><span class="fa fa-trash"></span></a></li>
    <li class="divider"></li>
  </ul>


<script>
     $(document).ready(function() {

        var url  = window.location.href;

        var c = localStorage.getItem('course');
        var sem = localStorage.getItem('semester');
        var y = localStorage.getItem('year')
        var url = window.location.href;

        $('.onremove').hide();


        if(c != null){
            $( "#course" ).val(c)

        }
        else{
            $( "#course" ).val('0')
        }

        if(sem != null){
            $( "#semester" ).val(sem)


        }
        else{
            $( "#semester" ).val('0')
        }

        if(y != null){
            $( "#year" ).val(y)

        }
        else{
            $( "#year" ).val('0')
        }

        $('.delete').click(function(){
            $('.onremove').show();
        });

        $(".course-option").click(function() {

            var id = $(this).data("id");
            var course = $(this).data("course");
            var description = $(this).data("description");
            var courseYear = $(this).data("year");
            
            $("#c_id").val(id);
            $("#course").val(course);
            $("#description").val(description);
            $("#course_year").val(courseYear);

        });


         $('.modal').modal();
         $('select').material_select();


        $(".lab").on("change paste keyup", function() {

            let total_hours = 0;
            let lec = $(".lec").val();
            let credit_units = 0;

            let lab = $(this).val();

            if(lab >= 1){
                credit_units = parseInt(lec) + parseInt(lab);
                total_hours = parseInt(lec) + parseInt(credit_units);
                $(".credit_units").val(credit_units);
                $(".total_hours").val(total_hours);

            }else{
                $(".total_hours").val(parseInt(lec));
                $(".credit_units").val(parseInt(lec));

            }
           console.log(lab);

        });

        $("#lab").on("change paste keyup", function() {

            let total_hours = 0;
            let lec = $("#lec").val();
            let credit_units = 0;

            let lab = $(this).val();

            if(lab >= 1){
                credit_units = parseInt(lec) + parseInt(lab);
                total_hours = parseInt(lec) + parseInt(credit_units);
                $("#credit_units").val(credit_units);
                $("#total_hours").val(total_hours);

            }else{
                $("#total_hours").val(parseInt(lec));
                $("#credit_units").val(parseInt(lec));

            }
           console.log(lab);

        });



        $( "#course" ).change(function() {

           var course = $(this).val();
           localStorage.setItem('course',course);
           window.location.href =   updateQueryStringParameter( url, 'course', course )

        });


        $( "#semester" ).change(function() {

           var semester = $(this).val();
           localStorage.setItem('semester',semester);
           window.location.href =   updateQueryStringParameter( url, 'semester', semester )

        });


         $( "#year" ).change(function() {

           var year = $(this).val();
           localStorage.setItem('year',year);
           window.location.href =   updateQueryStringParameter( url, 'year', year )

        });

         $('#search').keypress(function (e) {
          if (e.which == 13) {
             window.location.href = updateQueryStringParameter( url, 'search', $(this).val() )
          }
        });


        function updateQueryStringParameter(uri, key, value) {
              var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
              if( value === undefined ) {
                if (uri.match(re)) {
                    return uri.replace(re, '$1$2');
                } else {
                    return uri;
                }
              } else {
                if (uri.match(re)) {
                    return uri.replace(re, '$1' + key + "=" + value + '$2');
                } else {
                var hash =  '';
                if( uri.indexOf('#') !== -1 ){
                    hash = uri.replace(/.*#/, '#');
                    uri = uri.replace(/#.*/, '');
                }
                var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                return uri + separator + key + "=" + value + hash;
              }
              }
        }

    });
</script>
@endsection
