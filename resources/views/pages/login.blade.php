@extends('layouts.app')
@section('content')
@include('includes.toasts')
   <div class="form-container">
   	
		<div class="card">
			
		
			<div class="card-content">
				<h1 class="card-title center"><i class="fi-torso" aria-hidden="true"></i> Login </h1>
				{!! Form::open(['action' => 'PagesController@runLogin', 'method' => 'POST']) !!}
					<br>
					@include('includes.message')
					<input type="hidden" class="validate" name="user" value="student" required readonly>
			
					<div class="input-field">
						<input type="text" class="validate" name="email" required>
						<label>Username or Email Address</label>
						</div>
						<div class="input-field">
						<input type="password" class="validate" name="password" required>
						<label>Password</label>
						</div>
			
						<div class="card-action center">
						<button class="large fluid positive ui button" type="submit"><i class="fa fa-sign-in" aria-hidden="true"></i> Login
									</button>
						</div>
				{!! Form::close() !!}	
			
		</div>
	</div>
</div>

@endsection