@extends('layouts.app')

@section('content')
 <div class="section no-pad-bot showcase" id="index-banner">
    <div class="container center">
      <br><br>
      <img class="center" src="{{asset('img/logo.png')}}" alt="Lourdes College" width=150> </a>
      <h1 class="header center white-text"><strong>Lourdes College</strong></h1>
      <div class="row center">
        <h4 class="header white-text">Online Enrollment System</h4>
      </div>
        @if(session('logged_in'))

        @else
              <div class="row center">
                <a href="{{url('enrollment/options')}}" id="download-button" class="btn-large waves-effect waves-light pink">Enroll Now</a>
             </div>
        @endif

      <br><br>

    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col s12">
        <h3 class="center" style="margin-top: 30px; margin-bottom: 30px;"><i class="fa fa-graduation-cap" aria-hidden="true"></i> <strong>{{ 'Courses Offered' }}</strong></h3>
      </div>
      @foreach($courses as $course)
       <div class="col s4">
        <div class="card grey darken-3 hoverable">
          <div class="card-content white-text">
            <span class="card-title"><strong>{{ $course->course }}</strong></span>
            <p>{{ $course->description }}</p>
          </div>
          <div class="card-action">
            <a href="{{url('courses')}}/{{$course->course}}?semester=1&year=1" class="light-blue-text darken-3">View Subjects</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>


@endsection
