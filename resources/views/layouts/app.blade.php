<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="{{asset('img/logo.ico')}}">

          <!-- Semantic UI CSS -->
        <link rel="stylesheet" href="{{asset('css/table.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/message.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/button.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/menu.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/item.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/image.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/label.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/feed.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/breadcrumb.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/divider.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/form.min.css')}}">
        <link rel="stylesheet" href=" https://printjs-4de6.kxcdn.com/print.min.css">


  


        <!-- Styles -->
        <link rel="stylesheet" href="{{asset('css/global.css')}}">
        <link rel="stylesheet" href="{{asset('css/materialize.css')}}">
        <link rel="stylesheet" href="{{asset('css/materialize.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

        <!-- Icon fonts -->
        <link rel="stylesheet" href="{{asset('css/foundation-icons.css')}}">
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <!-- Scripts -->
        <script src="{{asset('js/app.js')}}"></script>
        <script src="{{asset('js/jquery-2.1.1.min.js')}}"></script>
         <script src="{{asset('js/dropdown.min.js')}}"></script>
        <script src="{{asset('js/materialize.js')}}"></script>
        <script src="{{asset('js/init.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>

        <title>Lourdes College Online Enrollment System</title>

    </head>
    <body>
        @include('includes.loader')
        @include('includes.header')
        @yield('content')
        <script>
            document.addEventListener("DOMContentLoaded", function(){
                $('.preloader-background').delay(1700).fadeOut('slow');

                $('.preloader-wrapper')
                    .delay(1700)
                    .fadeOut();
            });
        </script>
        @include('includes.footer')
    </body>

</html>
