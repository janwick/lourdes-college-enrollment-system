@component('mail::message')
<h1>Hello Good Day {{$name}}! Praise be Jesus and Mary</h1>
<h4> We have receive your registration request and we've verified you to enroll in Lourdes College. </h4>


@component('mail::button', ['url' => route('login')])
Enroll Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
