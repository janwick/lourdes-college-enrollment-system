@component('mail::message')
<h1>{{$payment_code}}</h1>
<h2>{{$header}}</h2>
<br><br>
<h4>{{$message}}</h4>

@component('mail::table')
 <table class="ui celled padded table attached">
      <thead>
          <th>Schedule Day</th>
          <th>Time</th>
          <th>Subject</th>
          <th>Room</th>
          <th>Faculty</th>
      </thead>

      <tbody>
         @foreach($schedules as $schedule)
               <tr>
                 <td>{{$schedule->schedule_day}}</td>
                 <td>{{Date('g:i A', strtotime($schedule->start_time))}} - {{Date('g:i A', strtotime($schedule->end_time))}}</td>
                 <td>{{$schedule->subject}}</td>
                 <td>{{$schedule->room}}</td>
                 <td>{{$schedule->faculty_name}}</td>
                </tr>
          @endforeach
      </tbody>
</table>
@endcomponent

@component('mail::button', ['url' => route('login')])
Check Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
