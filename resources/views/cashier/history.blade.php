@extends('layouts.app')
@section('content')

@include('includes.toasts')
<div class="container">
	<div class="row profile">
		<div class="col s12 text-center">
			<h3><strong>Payment Histories ({{ count($records) }})</strong></h3>
		</div>
		<div class="col s12">
			@if(count($records) > 0)
			<div class="collection">
				@foreach($records as $record)
					<a href="{{url('account/cashier/viewPaymentHistory')}}?email={{$record->student_email}}" class="collection-item">{{ $record->lastname }} {{ $record->firstname }} ( Click to view )</a>
				@endforeach
			</div>
			@else 
				<hr>
				<h4><center>No records yet </center></h4>
				<hr>
			@endif
		</div>
	</div>
</div>
@endsection