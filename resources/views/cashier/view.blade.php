@extends('layouts.app')
@section('content')

<br>
@include('includes.toasts')
<div class="container">
	<div class="row profile">
		<div class="col s12 text-center">
        <h4><strong>Payment Records to Verify > {{ $student->lastname }} {{ $student->firstname }}</strong></h4>
		</div>
		<div class="col s12">
				{!! Form::open(['action' => 'CashierController@confirmPayment', 'method' => 'POST'],['id' => 'verify-form']) !!}
				<input type="hidden" name="student_email" value="{{$student->email}}">
                <div class="card attached" style="padding-bottom:30px;">
                        <div class="ui message attached">
                              <div class="content">
                                <div class="header">
                                  <i class="fa fa-exchange" aria-hidden="true"></i> Student Assessment
                                </div>
                              </div>
                            </div>
                        <div class="card-content">
								<h5>Name: {{ $student->lastname }} {{ $student->firstname }} </h5> 
								<h5>Course and year: {{ $course->course }} - {{ $student->year_level }} </h5>
                                @if(count($schedules) > 0)
                                    <table class="ui celled padded table attached" style="margin-bottom: 2em;">
                                      <thead>
                                          <th>Schedule Day</th>
                                          <th>Time</th>
                                          <th>Subject</th>
                                          <th>Room</th>
										  <th>Amount</th>
                                      </thead>
                                  
                                      <tbody>
                                         @foreach($schedules as $schedule)
										 		<input type="hidden" value="{{$schedule->studsubject_id}}" name="studsubject_id[]">
                                               <tr>
                                                 <td>{{$schedule->schedule_day}}</td>
                                                 <td>{{Date('g:i A', strtotime($schedule->start_time))}} - {{Date('g:i A', strtotime($schedule->end_time))}}</td>
                                                 <td>{{$schedule->subject}}</td>
                                                 <td>{{$schedule->room}}</td>
												 <td>
												 	<div class="input-field" id="amounts">
													 	<input type="number" name="amount[]">
													 </div>
												 </td>
                                              </tr>
                                          @endforeach
                                      </tbody>
                                    </table>

									<div class="input-field">
										<input type="number" name="miscellaneous" id="miscellaneous">
						    			<label ">Miscellaneous Fees:</label>
									</div>

									<div class="row" style="margin-top: 20px margin-bottom: 20px;">
										<div class="col s6">
											<h3>Total Fees: <span class="totalFees"></span> </h3>
										</div>
									
										<div class="col s6">
											<button type="button" role="button" id="compute" class="waves-effect waves-light btn blue">Compute</button>
										</div>
									</div>

						
									<button type="submit" class="waves-effect waves-light btn green" style="float:right;">Confirm</button>
                              @else
                                  <div class="ui message warning">
                                  <div class="content">
                                    <div class="header">
                                      No Temporary schedules yet
                                    </div>
                                  </div>
                                </div>
                              @endif
                        </div>
                    </div>
					{!! Form::close() !!}
		</div>
	</div>
</div>




<script>
	$(document).ready(function() {
		var $amounts = $('#amounts input[type="number"]');
		var $miscellaneous = $('#miscellaneous');
		var $total = 0.00;
		var $oldTotal = 0.00;
		$('.totalFees').text($total);
		// $amounts.change(function(){
		// 	$amounts.each( function() {
		// 		if(this.value) {
		// 			$total += parseInt(this.value)
		// 		}
		// 	});
		// 	$('.totalFees').text($total);
		// });

		// $miscellaneous.change(function(){
		// 	$miscellaneous.each( function() {
		// 		if(this.value) {
		// 			$total += parseInt(this.value)
		// 		}
		// 	});
		// 	$('.totalFees').text($total);
		// });

		$('#compute').click(function()  {
			$amounts.each( function() {
				if(this.value) {
					$total += parseInt(this.value)
				}
			});
			$total += parseInt($miscellaneous.val());
			$('.totalFees').text($total);
			$('#totalFees').text($total);
		})

  	});
</script>
@endsection
