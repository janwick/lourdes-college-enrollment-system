@extends('layouts.app')
@section('content')

<br>
@include('includes.toasts')
<div class="container">
	<div class="row profile">
		<div class="col s12 text-center">
			<h3><strong>Payment Records to Verify ({{ count($records) }})</strong></h3>
		</div>
		<div class="col s12">
			@if(count($records) > 0)
			<div class="collection">
				@foreach($records as $record)
					<a href="{{url('account/cashier/viewRecord')}}?email={{$record->student_email}}" class="collection-item">{{ $record->lastname }} {{ $record->firstname }} ( Click to view )</a>
				@endforeach
			</div>
			@else 
				<hr>
				<h4><center>No records yet </center></h4>
				<hr>
			@endif
		</div>
	</div>
</div>
<script>
	 $(document).ready(function() {

	 	var url  = window.location.href;

	 	var c = localStorage.getItem('course');
	 	var sem = localStorage.getItem('semester');
	 	var y = localStorage.getItem('year')
	 	var url      = window.location.href;

	 	$('.onenroll').hide();
	 	$('.onreject').hide();

	 	$('.enroll').click(function(){
	 	 	$('.onenroll').show();
	 	 	$('.onreject').hide();
	 	});

	 	$('.reject').click(function(){
	 	 	$('.onenroll').hide();
	 	 	$('.onreject').show();
	 	});

	 	$('.cancel-enroll').click(function(){
	 	 	$('.onenroll').hide();
	 	});

	 	$('.cancel-reject').click(function(){
	 	 	$('.onreject').hide();
	 	});




	 	if(c != null){
			$( "#course" ).val(c)

		}
		else{
			$( "#course" ).val('0')
		}

		if(sem != null){
			$( "#semester" ).val(sem)


		}
		else{
			$( "#semester" ).val('0')
		}

		if(y != null){
			$( "#year" ).val(y)

		}
		else{
			$( "#year" ).val('0')
		}

	 	$(".subject").click(function() {
        	var id = $(this).data("subject_id");
        	var subject = $(this).data("subject");
        	$("#subject_id").val(id);
        	$("#sched-title").text(subject + " - schedule");
    	});

	 	 $('.modal').modal();
   		 $('select').material_select();


   		$("#lab").on("change paste keyup", function() {

          	let total_hours = 0;
          	let lec = $("#lec").val();
          	let credit_units = 0;

            let lab = $(this).val();

            if(lab >= 1){
            	credit_units = parseInt(lec) + parseInt(lab);
            	total_hours = parseInt(lec) + parseInt(credit_units);
            	$("#credit_units").val(credit_units);
            	$("#total_hours").val(total_hours);
            }else{
            	$("#total_hours").val(parseInt(lec));
            	$("#credit_units").val(parseInt(lec));
            }

        });



        $( "#course" ).change(function() {

           var course = $(this).val();
           localStorage.setItem('course',course);
           window.location.href =	updateQueryStringParameter( url, 'course', course )

        });


        $( "#semester" ).change(function() {

           var semester = $(this).val();
           localStorage.setItem('semester',semester);
           window.location.href =	updateQueryStringParameter( url, 'semester', semester )

        });


         $( "#year" ).change(function() {

           var year = $(this).val();
           localStorage.setItem('year',year);
           window.location.href =	updateQueryStringParameter( url, 'year', year )

        });

         $('#search').keypress(function (e) {
		  if (e.which == 13) {
		   	 window.location.href =	updateQueryStringParameter( url, 'search', $(this).val() )
		  }
		});


        function updateQueryStringParameter(uri, key, value) {
			  var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
			  if( value === undefined ) {
			  	if (uri.match(re)) {
					return uri.replace(re, '$1$2');
				} else {
					return uri;
				}
			  } else {
			  	if (uri.match(re)) {
			  		return uri.replace(re, '$1' + key + "=" + value + '$2');
				} else {
			    var hash =  '';
			    if( uri.indexOf('#') !== -1 ){
			        hash = uri.replace(/.*#/, '#');
			        uri = uri.replace(/#.*/, '');
			    }
			    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
			    return uri + separator + key + "=" + value + hash;
			  }
			  }
		}

  	});
</script>
@endsection
