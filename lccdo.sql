-- MySQL dump 10.16  Distrib 10.1.31-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: lourdes_enrollment_db
-- ------------------------------------------------------
-- Server version	10.1.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_countries`
--

DROP TABLE IF EXISTS `tbl_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_countries` (
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_countries`
--

LOCK TABLES `tbl_countries` WRITE;
/*!40000 ALTER TABLE `tbl_countries` DISABLE KEYS */;
INSERT INTO `tbl_countries` VALUES ('AD','Andorra'),('AE','United Arab Emirates'),('AF','Afghanistan'),('AG','Antigua and Barbuda'),('AI','Anguilla'),('AL','Albania'),('AM','Armenia'),('AN','Netherlands Antilles'),('AO','Angola'),('AQ','Antarctica'),('AR','Argentina'),('AT','Austria'),('AU','Australia'),('AW','Aruba'),('AZ','Azerbaijan'),('BA','Bosnia and Herzegovina'),('BB','Barbados'),('BD','Bangladesh'),('BE','Belgium'),('BF','Burkina Faso'),('BG','Bulgaria'),('BH','Bahrain'),('BI','Burundi'),('BJ','Benin'),('BM','Bermuda'),('BN','Brunei Darussalam'),('BO','Bolivia'),('BR','Brazil'),('BS','Bahamas'),('BT','Bhutan'),('BV','Bouvet Island'),('BW','Botswana'),('BY','Belarus'),('BZ','Belize'),('CA','Canada'),('CC','Cocos (Keeling) Islands'),('CF','Central African Republic'),('CG','Congo'),('CH','Switzerland'),('CI','Ivory Coast'),('CK','Cook Islands'),('CL','Chile'),('CM','Cameroon'),('CN','China'),('CO','Colombia'),('CR','Costa Rica'),('CU','Cuba'),('CV','Cape Verde'),('CX','Christmas Island'),('CY','Cyprus'),('CZ','Czech Republic'),('DE','Germany'),('DJ','Djibouti'),('DK','Denmark'),('DM','Dominica'),('DO','Dominican Republic'),('DS','American Samoa'),('DZ','Algeria'),('EC','Ecuador'),('EE','Estonia'),('EG','Egypt'),('EH','Western Sahara'),('ER','Eritrea'),('ES','Spain'),('ET','Ethiopia'),('FI','Finland'),('FJ','Fiji'),('FK','Falkland Islands (Malvinas)'),('FM','Micronesia, Federated States of'),('FO','Faroe Islands'),('FR','France'),('FX','France, Metropolitan'),('GA','Gabon'),('GB','United Kingdom'),('GD','Grenada'),('GE','Georgia'),('GF','French Guiana'),('GH','Ghana'),('GI','Gibraltar'),('GK','Guernsey'),('GL','Greenland'),('GM','Gambia'),('GN','Guinea'),('GP','Guadeloupe'),('GQ','Equatorial Guinea'),('GR','Greece'),('GS','South Georgia South Sandwich Islands'),('GT','Guatemala'),('GU','Guam'),('GW','Guinea-Bissau'),('GY','Guyana'),('HK','Hong Kong'),('HM','Heard and Mc Donald Islands'),('HN','Honduras'),('HR','Croatia (Hrvatska)'),('HT','Haiti'),('HU','Hungary'),('ID','Indonesia'),('IE','Ireland'),('IL','Israel'),('IM','Isle of Man'),('IN','India'),('IO','British Indian Ocean Territory'),('IQ','Iraq'),('IR','Iran (Islamic Republic of)'),('IS','Iceland'),('IT','Italy'),('JE','Jersey'),('JM','Jamaica'),('JO','Jordan'),('JP','Japan'),('KE','Kenya'),('KG','Kyrgyzstan'),('KH','Cambodia'),('KI','Kiribati'),('KM','Comoros'),('KN','Saint Kitts and Nevis'),('KP','Korea, Democratic People\'s Republic of'),('KR','Korea, Republic of'),('KW','Kuwait'),('KY','Cayman Islands'),('KZ','Kazakhstan'),('LA','Lao People\'s Democratic Republic'),('LB','Lebanon'),('LC','Saint Lucia'),('LI','Liechtenstein'),('LK','Sri Lanka'),('LR','Liberia'),('LS','Lesotho'),('LT','Lithuania'),('LU','Luxembourg'),('LV','Latvia'),('LY','Libyan Arab Jamahiriya'),('MA','Morocco'),('MC','Monaco'),('MD','Moldova, Republic of'),('ME','Montenegro'),('MG','Madagascar'),('MH','Marshall Islands'),('MK','Macedonia'),('ML','Mali'),('MM','Myanmar'),('MN','Mongolia'),('MO','Macau'),('MP','Northern Mariana Islands'),('MQ','Martinique'),('MR','Mauritania'),('MS','Montserrat'),('MT','Malta'),('MU','Mauritius'),('MV','Maldives'),('MW','Malawi'),('MX','Mexico'),('MY','Malaysia'),('MZ','Mozambique'),('NA','Namibia'),('NC','New Caledonia'),('NE','Niger'),('NF','Norfolk Island'),('NG','Nigeria'),('NI','Nicaragua'),('NL','Netherlands'),('NO','Norway'),('NP','Nepal'),('NR','Nauru'),('NU','Niue'),('NZ','New Zealand'),('OM','Oman'),('PA','Panama'),('PE','Peru'),('PF','French Polynesia'),('PG','Papua New Guinea'),('PH','Philippines'),('PK','Pakistan'),('PL','Poland'),('PM','St. Pierre and Miquelon'),('PN','Pitcairn'),('PR','Puerto Rico'),('PS','Palestine'),('PT','Portugal'),('PW','Palau'),('PY','Paraguay'),('QA','Qatar'),('RE','Reunion'),('RO','Romania'),('RS','Serbia'),('RU','Russian Federation'),('RW','Rwanda'),('SA','Saudi Arabia'),('SB','Solomon Islands'),('SC','Seychelles'),('SD','Sudan'),('SE','Sweden'),('SG','Singapore'),('SH','St. Helena'),('SI','Slovenia'),('SJ','Svalbard and Jan Mayen Islands'),('SK','Slovakia'),('SL','Sierra Leone'),('SM','San Marino'),('SN','Senegal'),('SO','Somalia'),('SR','Suriname'),('ST','Sao Tome and Principe'),('SV','El Salvador'),('SY','Syrian Arab Republic'),('SZ','Swaziland'),('TC','Turks and Caicos Islands'),('TD','Chad'),('TF','French Southern Territories'),('TG','Togo'),('TH','Thailand'),('TJ','Tajikistan'),('TK','Tokelau'),('TM','Turkmenistan'),('TN','Tunisia'),('TO','Tonga'),('TP','East Timor'),('TR','Turkey'),('TT','Trinidad and Tobago'),('TV','Tuvalu'),('TW','Taiwan'),('TY','Mayotte'),('TZ','Tanzania, United Republic of'),('UA','Ukraine'),('UG','Uganda'),('UM','United States minor outlying islands'),('US','United States'),('UY','Uruguay'),('UZ','Uzbekistan'),('VA','Vatican City State'),('VC','Saint Vincent and the Grenadines'),('VE','Venezuela'),('VG','Virgin Islands (British)'),('VI','Virgin Islands (U.S.)'),('VN','Vietnam'),('VU','Vanuatu'),('WF','Wallis and Futuna Islands'),('WS','Samoa'),('XK','Kosovo'),('YE','Yemen'),('ZA','South Africa'),('ZM','Zambia'),('ZR','Zaire'),('ZW','Zimbabwe');
/*!40000 ALTER TABLE `tbl_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_course`
--

DROP TABLE IF EXISTS `tbl_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(20) NOT NULL,
  `description` varchar(191) NOT NULL,
  `course_year` int(5) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_course`
--

LOCK TABLES `tbl_course` WRITE;
/*!40000 ALTER TABLE `tbl_course` DISABLE KEYS */;
INSERT INTO `tbl_course` VALUES (1,'BSA','Bachelor of Science in Accountancy',4,'2019-01-26 23:55:36','0000-00-00 00:00:00'),(2,'BSIT','Bachelor of Science in Information Technology',4,'2019-01-26 23:55:36','0000-00-00 00:00:00'),(3,'BSBA','Bachelor of Science in Business Administration - Financial Management',4,'2019-01-26 23:55:36','0000-00-00 00:00:00'),(5,'BSIS','Bachelor of Science in Information System',4,'2019-01-26 23:55:36','2018-11-07 19:33:48'),(6,'BSAIS','Bachelor of Science in Accounting Information System',4,'2019-01-26 23:55:36','2018-11-07 20:09:09'),(7,'BSBA','Bachelor of Science in Business Administration - Marketing Management',4,'2019-01-26 23:55:36','2018-11-07 20:22:09'),(8,'BSED - MF','Bachelor of Secondary Education Major in Filipino',4,'2019-01-26 23:55:36','2018-11-07 20:23:50'),(9,'BSED - SS','Bachelor of Secondary Education Major in Social Studies',4,'2019-01-26 23:55:36','2018-11-07 20:24:31'),(10,'BSED - MVE','Bachelor of Secondary Education Major in Values Education',4,'2019-01-26 23:55:36','2018-11-07 20:25:22'),(11,'BCAE','Bachelor of Culture and Arts Education',4,'2019-01-26 23:55:36','2018-11-07 20:25:53'),(12,'BTLE - HE','Bachelor of Technology & Livelihood Education Major in Home Economics',4,'2019-01-26 23:55:36','2018-11-07 20:28:55'),(13,'BSED - ME','Bachelor of Secondary Education Major in English',4,'2019-01-26 23:55:36','2018-11-07 20:29:25'),(14,'BECE','Bachelor of Early Childhood Education',4,'2019-01-26 23:55:36','2018-11-07 20:29:53'),(15,'BEE','Bachelor of Elementary Education',4,'2019-01-26 23:55:36','2018-11-07 20:30:07'),(16,'BSND','Bachelor of Science in Nutrition & Dietetics',4,'2019-01-26 23:55:36','2018-11-07 20:30:29'),(17,'BSN','Bachelor in Science in Nursing',4,'2019-01-26 23:55:36','2018-11-07 20:30:43'),(18,'BSPHARM','Bachelor of Science in Pharmacy',4,'2019-01-26 23:55:36','2018-11-07 20:31:12'),(19,'BSTM','Bachelor of Science in Tourism Management',4,'2019-01-26 23:55:36','2018-11-07 20:31:31'),(20,'BSHM','Bachelor of Science in Hospitality Management',4,'2019-01-26 23:55:36','2018-11-07 20:32:46'),(21,'BA-English Language','Bachelor of Arts in English Language',5,'2019-01-26 12:09:59','2018-11-07 20:33:12'),(22,'BA-Comm','Bachelor of Arts in Communication',4,'2019-01-26 23:55:36','2018-11-07 20:33:25'),(23,'BS-PSYCH','Bachelor of Science in Psychology',4,'2019-01-26 23:55:36','2018-11-07 20:33:59'),(24,'TST','TEST',5,'2019-01-26 12:11:34','2019-01-26 12:11:34');
/*!40000 ALTER TABLE `tbl_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_faculty`
--

DROP TABLE IF EXISTS `tbl_faculty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_faculty` (
  `faculty_id` int(11) NOT NULL AUTO_INCREMENT,
  `faculty_name` varchar(255) NOT NULL,
  PRIMARY KEY (`faculty_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_faculty`
--

LOCK TABLES `tbl_faculty` WRITE;
/*!40000 ALTER TABLE `tbl_faculty` DISABLE KEYS */;
INSERT INTO `tbl_faculty` VALUES (2,'Patrick Marlowe Oliva'),(3,'Luis Cadiz'),(5,'Johnbert Estroga'),(7,'Kurt Candillas'),(8,'Alexander Suan'),(9,'Loveth Mae Angcos'),(11,'Delia Pahang'),(12,'Rhandy Oyao'),(13,'Anthony Dagang'),(14,'Babes Reyes'),(16,'Rhea Ganas'),(17,'Noel Pit'),(18,'Rose Mary Cardenas'),(19,'Temporada'),(20,'Marilou Magadan');
/*!40000 ALTER TABLE `tbl_faculty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_notification`
--

DROP TABLE IF EXISTS `tbl_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `notification` text NOT NULL,
  `sent_from` varchar(255) NOT NULL,
  `sent_to` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_sent` datetime NOT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_notification`
--

LOCK TABLES `tbl_notification` WRITE;
/*!40000 ALTER TABLE `tbl_notification` DISABLE KEYS */;
INSERT INTO `tbl_notification` VALUES (81,'Subjects you requested for enrollment evaluated successfully and already submitted to registrar for confirmation.','rhandy','jantinn.webdeveloper@gmail.com',1,0,'2018-11-11 15:17:44'),(83,'Subjects you requested for enrollment evaluated successfully and already submitted to registrar for confirmation.','rhandy','jantinn.webdeveloper@gmail.com',1,0,'2018-11-21 11:18:10'),(87,'Subjects you requested for enrollment evaluated successfully and already submitted to registrar for confirmation.','rhandy','jantinn.webdeveloper@gmail.com',1,0,'2018-11-21 12:35:53'),(89,'Subjects you requested for enrollment evaluated successfully and already submitted to registrar for confirmation.','rhandy','jantinn.webdeveloper@gmail.com',1,0,'2018-11-21 12:53:19'),(90,'You\'re enrollment request is already processed, please check your email for the payment details and instructions.','admin2018','jantinn.webdeveloper@gmail.com',1,0,'2018-11-21 12:54:15'),(91,'You are now enrolled!','admin2018','jantinn.webdeveloper@gmail.com',1,0,'2018-11-21 12:55:52'),(92,'Jan Erezo submitted 2 subjects for evaluation','erezojantinn@gmail.com','erezojantinn@gmail.com',2,0,'2019-01-27 01:19:57');
/*!40000 ALTER TABLE `tbl_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_payment`
--

DROP TABLE IF EXISTS `tbl_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_payment` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_code` varchar(20) NOT NULL,
  `request_id` varchar(50) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`payment_id`),
  UNIQUE KEY `payment_code` (`payment_code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_payment`
--

LOCK TABLES `tbl_payment` WRITE;
/*!40000 ALTER TABLE `tbl_payment` DISABLE KEYS */;
INSERT INTO `tbl_payment` VALUES (6,'998967359','27,28',1,'2018-11-11 03:52:02','2018-11-10 15:52:02'),(7,'915161207','27,28',1,'2018-11-11 03:57:02','2018-11-10 15:57:02'),(8,'213638123','27,28',1,'2018-11-11 03:59:50','2018-11-10 15:59:50'),(9,'858891849','11,12',1,'2018-11-21 12:55:52','2018-11-21 00:55:52');
/*!40000 ALTER TABLE `tbl_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_prereq`
--

DROP TABLE IF EXISTS `tbl_prereq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_prereq` (
  `prereq_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) NOT NULL,
  `prereqsubject_id` int(11) NOT NULL,
  PRIMARY KEY (`prereq_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_prereq`
--

LOCK TABLES `tbl_prereq` WRITE;
/*!40000 ALTER TABLE `tbl_prereq` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_prereq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_schedule`
--

DROP TABLE IF EXISTS `tbl_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_schedule` (
  `schedule_id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_day` varchar(50) NOT NULL,
  `semester` int(1) NOT NULL,
  `school_year` varchar(9) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `subject_id` int(11) NOT NULL,
  `room` varchar(100) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `slots` int(2) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`schedule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_schedule`
--

LOCK TABLES `tbl_schedule` WRITE;
/*!40000 ALTER TABLE `tbl_schedule` DISABLE KEYS */;
INSERT INTO `tbl_schedule` VALUES (27,'MTH',1,'2018-2019','09:00:00','10:30:00',145,'CompLab',2,9,1,'2019-01-27 01:19:57','2018-11-08 18:36:20'),(28,'MTH',1,'2018-2019','01:00:00','02:30:00',143,'207',9,31,1,'2019-01-27 01:19:57','2018-11-08 18:37:02'),(29,'TF',1,'2018-2019','17:03:00','19:00:00',148,'800',7,42,1,'2018-11-21 12:34:57','2018-11-21 00:04:46'),(30,'SAT',1,'2018-2019','13:00:00','16:00:00',145,'250',2,45,1,'2018-11-21 00:11:44','2018-11-21 00:11:44'),(31,'WED',1,'2018-2019','10:30:00','12:00:00',147,'101',11,29,1,'2018-11-21 12:52:13','2018-11-21 00:47:25');
/*!40000 ALTER TABLE `tbl_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_schoolyear`
--

DROP TABLE IF EXISTS `tbl_schoolyear`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_schoolyear` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `school_year` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_schoolyear`
--

LOCK TABLES `tbl_schoolyear` WRITE;
/*!40000 ALTER TABLE `tbl_schoolyear` DISABLE KEYS */;
INSERT INTO `tbl_schoolyear` VALUES (1,'2019-2020',1,'2019-01-26 11:35:50','2019-01-25 23:35:50'),(2,'2020-2021',0,'2019-01-26 11:35:50','2019-01-25 23:35:50'),(3,'2021-2022',0,'2019-01-26 10:39:02','2019-01-25 22:34:01'),(4,'2022-2023',0,'2019-01-25 22:39:14','2019-01-25 22:39:14');
/*!40000 ALTER TABLE `tbl_schoolyear` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_semester`
--

DROP TABLE IF EXISTS `tbl_semester`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_semester` (
  `semester` int(1) NOT NULL AUTO_INCREMENT,
  `description` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`semester`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_semester`
--

LOCK TABLES `tbl_semester` WRITE;
/*!40000 ALTER TABLE `tbl_semester` DISABLE KEYS */;
INSERT INTO `tbl_semester` VALUES (1,'First semester',1),(2,'Second semester',0),(3,'Summer',0);
/*!40000 ALTER TABLE `tbl_semester` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_students`
--

DROP TABLE IF EXISTS `tbl_students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_students` (
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `course_id` int(11) NOT NULL,
  `year_level` int(1) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `religion` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `place_of_birth` varchar(255) NOT NULL,
  `civil_status` varchar(10) NOT NULL,
  `home_address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `country_code` varchar(5) NOT NULL,
  `zipcode` int(10) NOT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `guardian` varchar(255) NOT NULL,
  `guardian_relationship` varchar(255) NOT NULL,
  `basic_education` varchar(255) NOT NULL,
  `secondary_education` varchar(255) NOT NULL,
  `college_education` varchar(255) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `enrolled` tinyint(1) NOT NULL,
  `date_enrolled` date NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_students`
--

LOCK TABLES `tbl_students` WRITE;
/*!40000 ALTER TABLE `tbl_students` DISABLE KEYS */;
INSERT INTO `tbl_students` VALUES ('erezojantinn@gmail.com','erezo','noimage.jpg','Erezo','Jan','Cuanag',2,1,'Male','Roman Catholic','Filipino','2019-01-09','cdo','Single','dgdfg','Cebu City','fgdfgdfgdg','AD',9000,'798989879','test','test','gdfgdfg','sdfsdfd','ets',0,0,'2019-01-26','2019-01-25 23:50:39','2019-01-25 23:50:39');
/*!40000 ALTER TABLE `tbl_students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_studsubjects`
--

DROP TABLE IF EXISTS `tbl_studsubjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_studsubjects` (
  `studsubject_id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `student_email` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`studsubject_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_studsubjects`
--

LOCK TABLES `tbl_studsubjects` WRITE;
/*!40000 ALTER TABLE `tbl_studsubjects` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_studsubjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_subcourse`
--

DROP TABLE IF EXISTS `tbl_subcourse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_subcourse` (
  `subcourse_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`subcourse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_subcourse`
--

LOCK TABLES `tbl_subcourse` WRITE;
/*!40000 ALTER TABLE `tbl_subcourse` DISABLE KEYS */;
INSERT INTO `tbl_subcourse` VALUES (1,140,2),(2,140,3),(3,141,1),(4,141,2),(5,141,3),(6,142,1),(7,142,2),(8,142,3),(9,142,5),(10,142,6),(11,142,7),(12,142,8),(13,142,9),(14,142,10),(15,142,11),(16,142,12),(17,142,13),(18,142,14),(19,142,15),(20,142,16),(21,142,17),(22,142,18),(23,142,19),(24,142,20),(25,142,21),(26,142,22),(27,142,23),(28,143,1),(29,143,2),(30,143,3),(31,143,5),(32,143,6),(33,143,7),(34,143,8),(35,143,9),(36,143,10),(37,143,11),(38,143,12),(39,143,13),(40,143,14),(41,143,15),(42,143,16),(43,143,17),(44,143,18),(45,143,19),(46,143,20),(47,143,21),(48,143,22),(49,143,23),(50,144,1),(51,144,2),(52,144,3),(53,144,5),(54,144,6),(55,144,7),(56,144,8),(57,144,9),(58,144,10),(59,144,11),(60,144,12),(61,144,13),(62,144,14),(63,144,15),(64,144,16),(65,144,17),(66,144,18),(67,144,19),(68,144,20),(69,144,21),(70,144,22),(71,144,23),(72,145,2),(73,146,1),(74,146,2),(75,146,3),(76,146,7),(77,147,1),(78,147,2),(79,147,3),(80,147,5),(81,147,6),(82,147,7),(83,147,8),(84,147,9),(85,147,10),(86,147,11),(87,147,12),(88,147,13),(89,147,14),(90,147,15),(91,147,16),(92,147,17),(93,147,18),(94,147,19),(95,147,20),(96,147,21),(97,147,22),(98,147,23),(99,148,1),(100,148,2),(101,148,3);
/*!40000 ALTER TABLE `tbl_subcourse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_subjects`
--

DROP TABLE IF EXISTS `tbl_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_subjects` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `descriptive` varchar(255) NOT NULL,
  `lec` varchar(20) NOT NULL,
  `lab` varchar(20) NOT NULL,
  `credit_units` varchar(20) NOT NULL,
  `pre_reqs` varchar(100) NOT NULL,
  `total_hours` double NOT NULL,
  `semester` int(1) NOT NULL,
  `year_level` int(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_subjects`
--

LOCK TABLES `tbl_subjects` WRITE;
/*!40000 ALTER TABLE `tbl_subjects` DISABLE KEYS */;
INSERT INTO `tbl_subjects` VALUES (143,'RS 1','Scrip. & Old Test. Salvation History','3','0','3','None',3,1,1,'2018-11-08 18:21:57','2018-11-08 18:21:57'),(144,'Hist 1','Phil History w/Politics & Government','3','0','3','None',3,1,1,'2018-11-08 18:23:50','2018-11-08 18:23:50'),(145,'IT101','IT Fundamentals','2','1','3','None',5,1,1,'2018-11-08 18:24:47','2018-11-08 18:24:47'),(146,'Acctg 1','Accounting Principles','3','0','3','Math 2',3,1,4,'2018-11-08 18:28:58','2018-11-08 18:28:58'),(147,'Math 1','College Algebra','3','0','3','None',3,2,1,'2018-11-08 18:32:23','2018-11-08 18:32:23'),(148,'Algorithm 101','Test Description','3','5','8','None',11,1,1,'2018-11-11 01:12:03','2018-11-11 01:12:03');
/*!40000 ALTER TABLE `tbl_subjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_subreq`
--

DROP TABLE IF EXISTS `tbl_subreq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_subreq` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `student_email` varchar(255) NOT NULL,
  `evaluated` int(1) NOT NULL,
  `standing` int(1) NOT NULL,
  `request_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_subreq`
--

LOCK TABLES `tbl_subreq` WRITE;
/*!40000 ALTER TABLE `tbl_subreq` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_subreq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tempsubjects`
--

DROP TABLE IF EXISTS `tbl_tempsubjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tempsubjects` (
  `temp_id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) NOT NULL,
  `student_email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`temp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tempsubjects`
--

LOCK TABLES `tbl_tempsubjects` WRITE;
/*!40000 ALTER TABLE `tbl_tempsubjects` DISABLE KEYS */;
INSERT INTO `tbl_tempsubjects` VALUES (1,28,'erezojantinn@gmail.com','2019-01-26 13:41:15','2019-01-26 13:41:15'),(2,27,'erezojantinn@gmail.com','2019-01-26 13:41:15','2019-01-26 13:41:15'),(3,29,'erezojantinn@gmail.com','2019-01-26 13:42:57','2019-01-26 13:42:57');
/*!40000 ALTER TABLE `tbl_tempsubjects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_user` (
  `username` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `name` varchar(50) NOT NULL,
  `user_type` int(1) NOT NULL,
  `course_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES ('admins@gmail.com','$2y$10$4PkVmUlAz2gd9bPmMXUBLenmUGqFyDxC2me3bn05UdpSRW7YVz.Zq','jan',1,0,'2019-01-25 21:32:38','2019-01-25 21:32:38'),('erezojantinn@gmail.com','$2y$10$T0LLWvuf2coXZ7nuXPyFCupzUrevLTcFhvGGuO3tCeetcZzwuhD/y','Jan Erezo',4,2,'2019-01-25 23:50:39','2019-01-25 23:50:39'),('test@gmail.com','$2y$10$.qltMRI.1reyeQJY7MilLumovmfksKpwit7M37iQyTIJbsfp39t2S','jan',1,0,'2019-01-25 21:34:40','2019-01-25 21:34:40');
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-28  6:35:54
