Installation Instructions
--------------------------
1. run this command kung asa ninyo gusto ibutang ang project: git clone https://gitlab.com/janwick/lourdes-college-enrollment-system.git

2. after ana clone kay sulod or cd lourdes-college-enrollment-system and i-import ang lourdes_enrollment_db.sql sa lourdes_enrollment_db nga database na gihimo sa phpmyadmin - naa sa folder nga INSTRUCTIONS AND ERD, DBS ang .sql nga file

3. after ana run this command: cp .env.example .env

4. composer install

5. php artisan key:generate

6. php artisan serve



ERD and Database structure kay naa sa folder INSTRUCTIONS AND ERD, DBS.
Palihug dayon ko basa sa readme.txt ana nga folder and keep in touch with me unya kung unsay problema sa chat lang. Matsalamaaats! pasensya jud kaayo kulang kaayo sa time bai..

